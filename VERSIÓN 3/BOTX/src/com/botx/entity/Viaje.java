package com.botx.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the viaje database table.
 * 
 */
@Entity
@NamedQuery(name="Viaje.findAll", query="SELECT v FROM Viaje v")
public class Viaje implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idViaje;
	private String destino;
	private Date fecha;
	private Time horaFinal;
	private Time horaInicio;
	private int idTipoVehiculo;
	private String origen;
	private BigDecimal precio;
	private List<DetalleViaje> detalleViajes;
	private Cliente cliente;
	private Conductor conductor;
	private Estado estadoBean;

	public Viaje() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_viaje")
	public int getIdViaje() {
		return this.idViaje;
	}

	public void setIdViaje(int idViaje) {
		this.idViaje = idViaje;
	}


	public String getDestino() {
		return this.destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}


	@Temporal(TemporalType.DATE)
	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	@Column(name="hora_final")
	public Time getHoraFinal() {
		return this.horaFinal;
	}

	public void setHoraFinal(Time horaFinal) {
		this.horaFinal = horaFinal;
	}


	@Column(name="hora_inicio")
	public Time getHoraInicio() {
		return this.horaInicio;
	}

	public void setHoraInicio(Time horaInicio) {
		this.horaInicio = horaInicio;
	}


	@Column(name="id_tipo_vehiculo")
	public int getIdTipoVehiculo() {
		return this.idTipoVehiculo;
	}

	public void setIdTipoVehiculo(int idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}


	public String getOrigen() {
		return this.origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}


	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}


	//bi-directional many-to-one association to DetalleViaje
	@OneToMany(mappedBy="viaje")
	public List<DetalleViaje> getDetalleViajes() {
		return this.detalleViajes;
	}

	public void setDetalleViajes(List<DetalleViaje> detalleViajes) {
		this.detalleViajes = detalleViajes;
	}

	public DetalleViaje addDetalleViaje(DetalleViaje detalleViaje) {
		getDetalleViajes().add(detalleViaje);
		detalleViaje.setViaje(this);

		return detalleViaje;
	}

	public DetalleViaje removeDetalleViaje(DetalleViaje detalleViaje) {
		getDetalleViajes().remove(detalleViaje);
		detalleViaje.setViaje(null);

		return detalleViaje;
	}


	//bi-directional many-to-one association to Cliente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_cliente")
	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	//bi-directional many-to-one association to Conductor
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_conductor")
	public Conductor getConductor() {
		return this.conductor;
	}

	public void setConductor(Conductor conductor) {
		this.conductor = conductor;
	}


	//bi-directional many-to-one association to Estado
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="estado")
	public Estado getEstadoBean() {
		return this.estadoBean;
	}

	public void setEstadoBean(Estado estadoBean) {
		this.estadoBean = estadoBean;
	}

}