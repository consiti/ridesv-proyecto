package com.botx.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.botx.entity.Correo;
import com.botx.dao.AbstractFacade;
import com.botx.util.*;


public class CorreoFacade extends AbstractFacade<Correo> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public CorreoFacade() {
        super(Correo.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
	
	public List<Correo> getCorreoByEstado0ANDBotX(){
		
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createQuery("SELECT c FROM Correo c WHERE c.estado = :estado AND c.bot = :bot");
		query.setParameter("estado", 0);
		query.setParameter("bot", "X");
		
		try {
			return (List<Correo>)query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}
		
	}
	

}
