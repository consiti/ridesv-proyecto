package com.botx.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.botx.entity.ConductorPosicion;
import com.botx.util.JpaUtil;

public class ConductorPosicionFacade extends AbstractFacade<ConductorPosicion> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public ConductorPosicionFacade() {
        super(ConductorPosicion.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
    
    public List<ConductorPosicion> getConductorPosicionByEstado(int estado) {
		
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createQuery("SELECT c FROM ConductorPosicion c WHERE c.estado = :estado");
		query.setParameter("estado", estado);
		
		try {
			return (List<ConductorPosicion>)query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}		
		
	}

}
