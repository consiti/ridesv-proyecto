package com.botx.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.botx.entity.Conductor;
import com.botx.util.JpaUtil;
import com.botx.dao.AbstractFacade;

public class ConductorFacade extends AbstractFacade<Conductor> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public ConductorFacade() {
        super(Conductor.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
    
    public Conductor getConductorByTelefono(String telefono) {
		
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createQuery("SELECT c FROM Conductor c WHERE c.telefono = :telefono");
		query.setParameter("telefono", telefono);
		
		try {
			return (Conductor)query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}		
		
	}

}
