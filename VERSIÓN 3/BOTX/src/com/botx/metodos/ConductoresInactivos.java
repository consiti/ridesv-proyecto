package com.botx.metodos;

import java.util.Date;
import java.util.List;

import com.botx.dao.ConductorPosicionFacade;
import com.botx.entity.ConductorPosicion;

public class ConductoresInactivos {
	
	public boolean run() {
		
		//Instancia de las consultas 
		ConductorPosicionFacade conductorPosicionFacade = new ConductorPosicionFacade();
		ConductorPosicion conductorPosicion = new ConductorPosicion();
		
		//Se obtiene la lista de conductores que estan activos en la tabla <ConductorPosicion>
		List<ConductorPosicion> listaConductorPosicion = conductorPosicionFacade.getConductorPosicionByEstado(1);
		if(listaConductorPosicion.size() > 0) {
			
			for (int i = 0; i < listaConductorPosicion.size(); i++) {

				Date fechaActual = new Date();
				
				//Se compara si el tiempo en que enviaron su ultima posicion es mayor de 5 minutos = 300 segundos
				if(((fechaActual.getTime() - listaConductorPosicion.get(i).getHora().getTime())/1000) > 300) {
					
					//Se cambia el estado del conductor a inactivo 
					conductorPosicion = listaConductorPosicion.get(i);
					conductorPosicion.setEstado(0);
					conductorPosicionFacade.edit(conductorPosicion);
					
					//Salida en pantalla
					System.out.println("-------------CAMBIO DE ESTADO DE CONDUCTOR POR INACTIVIDAD-------------");
					System.out.println("ID CONDUCTOR = "+listaConductorPosicion.get(i).getIdPosicion());
					System.out.println("TIEMPO DE INACTIVIDAD (Segundos) = "+((fechaActual.getTime() - listaConductorPosicion.get(i).getHora().getTime())/1000));
					System.out.println();
					
				}
				
			}
			return true;
			
		}else {
			System.out.println("NO SE AN ENCONTRADO CONDUCTORES INACTIVOS");
			return false;
		}
	}
	

}
