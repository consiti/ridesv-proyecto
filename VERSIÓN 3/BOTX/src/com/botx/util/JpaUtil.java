package com.botx.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	private static final  String PERSISTENCE_UNIT_NAME = "BOTX-JPA-V1-PU";
	private static EntityManagerFactory factory;
	public static EntityManagerFactory getEntityManagerFactory(){	
		if(factory == null) {
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return factory;
	}
}
