package com.botx.main;

import java.util.Timer;
import java.util.TimerTask;

public class MainBOTX {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		final Timer timer = new Timer();
		System.out.println("INICIO DEL BOT X");
		
        TimerTask task = new TimerTask() {
            int cont = 0;
            
            CoreBOTX coreBOTX = new CoreBOTX();
            @Override public void run() {
                cont++;
                System.out.println("Iteracion = " + cont);
                coreBOTX.run();
            }
        };
        timer.schedule(task, 0, 5000);
        
	}

}
