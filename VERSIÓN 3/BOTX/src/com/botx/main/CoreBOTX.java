package com.botx.main;

import java.util.Date;
import java.util.List;

import com.botx.dao.ConductorFacade;
import com.botx.dao.ConductorPosicionFacade;
import com.botx.dao.CorreoFacade;
import com.botx.entity.Conductor;
import com.botx.entity.ConductorPosicion;
import com.botx.entity.Correo;
import com.botx.metodos.*;

public class CoreBOTX {
	
	public void run() {
		
		//Deshabilita los conductores que no esten enviado su posicion
		ConductoresInactivos conductoresInactivos = new ConductoresInactivos();
		conductoresInactivos.run();
		
		
		//Instancia de las consultas
		CorreoFacade correoFacade = new CorreoFacade();
		ConductorFacade conductorFacade = new ConductorFacade();
		ConductorPosicionFacade conductorPosicionFacade = new ConductorPosicionFacade();
		
		//Se obtiene la lista de la tabla <Correo> de los correos no leidos que coresponden al bot x
		List<Correo> listaCorreo = correoFacade.getCorreoByEstado0ANDBotX();
		if(listaCorreo.size() > 0) {
			
			for (int i = 0; i < listaCorreo.size(); i++) {
				
				//Se separa el cuerpo del email por comas
				String[] cuerpoEmailCortado =  listaCorreo.get(i).getCuerpo().trim().split(",");
				
				//Se realiza una consulta para obtener el id del conductor
				Conductor conductor = conductorFacade.getConductorByTelefono(cuerpoEmailCortado[2].trim());
				if(conductor != null) {
					
					//Se realisa la consulta para obtener el conductor en la tabla <ConductorPosicion>
					ConductorPosicion conductorPosicion = conductorPosicionFacade.find(conductor.getIdConductor());
					if(conductorPosicion != null) {
						
						//Se Actualiza la tabla del <ConductorPosicion>
						conductorPosicion.setUltimaPosicion(cuerpoEmailCortado[0].trim()+","+cuerpoEmailCortado[1].trim());
						//NO ACTULAIZA el estado de los conductores si tiene alguno de los estados 
						//<Proceso de viaje = 2>
						//<Bloqueado = 3>
						//<En espera de respuesta = 4>
						if(conductorPosicion.getEstado() < 2){
							conductorPosicion.setEstado(1);
						}
						conductorPosicion.setHora(new Date());
						conductorPosicionFacade.edit(conductorPosicion);
						
						//SALIDA EN PANTALLA
						System.out.println("-------------EXITO AL ACTUALIZAR LA TABLA <ConductorPosicion>-------------");
						System.out.println("CONDUCTOR ID = "+conductor.getIdConductor());
						System.out.println("CONDUCTOR NONMBRE = "+conductor.getNombre()+"-"+conductor.getApellido());
						System.out.println("CONDUCTOR POSICION = "+conductorPosicion.getUltimaPosicion());
						System.out.println();
						
						//Se Actualiza la tabla del <Correo> como leido
						Correo correo = listaCorreo.get(i);
						correo.setEstado(1);
						correoFacade.edit(correo);
						
						//SALIDA EN PANTALLA
						System.out.println("-------------EXITO AL ACTUALIZARLA TABLA <Correo>-------------");
						System.out.println("CORREO ID = "+correo.getIdcorreos());
						System.out.println("CORREO NO LEIDO a LEIDO = "+correo.getEstado());
						System.out.println();
						
					}else {
						System.out.println("ERROR = No se pudo encontrar el conductor en la tabla <ConductorPosicion> por el ID = "+conductor.getIdConductor());
					}
					
					
				}else {
					System.out.println("ERROR = No se pudo encontrar el conductor por el telefono = "+cuerpoEmailCortado[2].trim());
				}
				
			}	
			
		}else {
			System.out.println("WARNING = No se encontraron correos nuevos para el BOT X");
		}
		
	}

}
