package com.bot4.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.bot4.entity.Conductor;
import com.bot4.entity.Estado;
import com.bot4.entity.Viaje;
import com.bot4.util.JpaUtil;

public class ViajeFacade extends AbstractFacade<Viaje> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public ViajeFacade() {
        super(Viaje.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
    
    public Viaje getViajeByIdConductor(int idConductor) {
		
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createQuery("SELECT c FROM Viaje c WHERE c.conductor = :conductor AND c.estadoBean = :estado");
		query.setParameter("conductor", new Conductor(idConductor));
		query.setParameter("estado", new Estado(4));
		
		try {
			return (Viaje)query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}		
		
	}

}
