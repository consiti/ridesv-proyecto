package com.bot4.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.bot4.entity.Correo;
import com.bot4.dao.AbstractFacade;
import com.bot4.util.*;


public class CorreoFacade extends AbstractFacade<Correo> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public CorreoFacade() {
        super(Correo.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
	
	public List<Correo> getCorreoByEstado0ANDBot4(){
		
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createQuery("SELECT c FROM Correo c WHERE c.estado = :estado AND c.bot = :bot");
		query.setParameter("estado", 0);
		query.setParameter("bot", "4");
		
		try {
			return (List<Correo>)query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}
		
	}
	

}
