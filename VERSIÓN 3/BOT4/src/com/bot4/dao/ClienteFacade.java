package com.bot4.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.bot4.entity.Cliente;
import com.bot4.util.JpaUtil;

public class ClienteFacade extends AbstractFacade<Cliente> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public ClienteFacade() {
        super(Cliente.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }

}
