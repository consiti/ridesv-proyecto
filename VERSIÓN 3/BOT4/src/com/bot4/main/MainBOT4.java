package com.bot4.main;

import java.util.Timer;
import java.util.TimerTask;


public class MainBOT4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		final Timer timer = new Timer();
		System.out.println("INICIO DEL BOT 4");
		
        TimerTask task = new TimerTask() {
            int cont = 0;
            
            CoreBOT4 coreBOT4 = new CoreBOT4();
            @Override public void run() {
                cont++;
                System.out.println("Iteracion = " + cont);
                coreBOT4.run();
            }
        };
        timer.schedule(task, 0, 5000);

	}

}
