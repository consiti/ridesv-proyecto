package com.bot4.email;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.bot4.dao.ClienteFacade;
import com.bot4.entity.Cliente;
import com.bot4.entity.Viaje;

import eu.bitm.NominatimReverseGeocoding.NominatimReverseGeocodingJAPI;

import com.bot4.entity.Conductor;

public class EmailFeedBack {
	
	public static void sendEmail(Viaje viaje,Conductor conductor) {
		
		final String CORREO = "info.ridesv2019@gmail.com";
		final String CONTRASENNA = "Consiti2018";
		final String ASUNTO = "ride-viaje-feedback";
		
		ClienteFacade clienteFacade = new ClienteFacade();
		Cliente cliente = clienteFacade.find(viaje.getCliente().getIdCliente());
		String cuerpoCorreo = "";
		String correoDestino = cliente.getEmail();
		NominatimReverseGeocodingJAPI reverseGeocoding = new NominatimReverseGeocodingJAPI();
		String[] viajeOrigenCortado = viaje.getOrigen().trim().split(",");
		String[] viajeDestinoCortado = viaje.getDestino().trim().split(",");
		
		
		//Creacion del cuerpo del correo
		cuerpoCorreo = cuerpoCorreo+" - - - RIDE-VIAJE-FINALIZADO-No-"+viaje.getIdViaje()+" - - - \n\n";
		cuerpoCorreo = cuerpoCorreo+"FECHA DEL VIAJE : "+viaje.getFecha()+"\n";
		cuerpoCorreo = cuerpoCorreo+"PRECIO DEL VIAJE : "+viaje.getPrecio()+"\n";
		cuerpoCorreo = cuerpoCorreo+"ORIGEN DEL VIAJE : "+reverseGeocoding.getAdress(Double.valueOf(viajeOrigenCortado[0].trim()), Double.valueOf(viajeOrigenCortado[1].trim()))+"\n";
		cuerpoCorreo = cuerpoCorreo+"DESTINO DEL VIAJE : "+reverseGeocoding.getAdress(Double.valueOf(viajeDestinoCortado[0].trim()), Double.valueOf(viajeDestinoCortado[1].trim()))+"\n";
		cuerpoCorreo = cuerpoCorreo+"NONBRE DEL CONDUCTOR : "+conductor.getNombre()+" "+conductor.getApellido()+"\n";
		cuerpoCorreo = cuerpoCorreo+"TIPO DE VEHICULO : <MARCA>"+conductor.getVehiculo().getMarca()+"<MODELO>"+conductor.getVehiculo().getModelo()+"\n\n";
		
		cuerpoCorreo = cuerpoCorreo+" - - - Estimado "+cliente.getNombre()+" "+cliente.getApellido()+" - - - \n\n";
		cuerpoCorreo = cuerpoCorreo+"Gracias por usar RIDE como su medio de transporte nos gustar�a tener "
				+ "tu opini�n lo invitamos a responder este correo con sus comentarios\n";
		cuerpoCorreo = cuerpoCorreo+"Gracias de antemano que pase un feliz d�a";
		
		//configuracion para enviar correo
	    final String username = CORREO;
		final String password = CONTRASENNA;
	
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
	
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			}
		);
	
		try {
	
		      // Define message
		      MimeMessage message = new MimeMessage(session);
		      message.setFrom(new InternetAddress(username));
		      message.setSubject(ASUNTO+"-"+viaje.getIdViaje());
		      message.addRecipient(Message.RecipientType.TO,new InternetAddress(correoDestino));
		      message.setText(cuerpoCorreo);
		      
		      // Envia el mensaje
		      Transport.send(message);
		      
		} catch (Exception e) {
		    e.printStackTrace();
		}	
		
	}	

}
