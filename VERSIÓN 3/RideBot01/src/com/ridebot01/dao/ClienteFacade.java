package com.ridebot01.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.ridebot01.entity.Cliente;
import com.ridebot01.util.JpaUtil;

public class ClienteFacade extends AbstractFacade<Cliente> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ClienteFacade() {
		super(Cliente.class);
		getEntityManager();
	}


	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
	
	//Busca cliente usando como par�metro n�mero telef�nico
	public Cliente findByPhonenumber(String telefono){
		Cliente cli = new Cliente();
		
		try {
			TypedQuery<Cliente> q = em.createNamedQuery("Cliente.findByTelefono", Cliente.class);
	        q.setParameter("telefono", telefono);
	        cli = q.getResultList().get(0);
		} catch (Exception e) {
			cli = null;
			e.printStackTrace();
		}
        
        return cli;
    }
}