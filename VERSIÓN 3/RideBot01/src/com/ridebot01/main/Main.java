package com.ridebot01.main;

import java.util.Timer;
import java.util.TimerTask;

public class Main {

	public static void main(String[] args) {

		final Timer timer = new Timer();
		SolicitudViaje solicitud = new SolicitudViaje();

		TimerTask task = new TimerTask() {
			int conta = 0;

			@Override
			public void run() {
				conta++;
				System.out.println("BOT-1 Iteración " + conta);
				// Verificando solicitudes
				solicitud.atiendeSolicitud();
			}
		};

		timer.schedule(task, 0, 5000);
	}
}