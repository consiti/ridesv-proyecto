package com.ridebot01.main;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.ridebot01.entity.Viaje;
import com.ridebot01.entity.Correo;
import com.ridebot01.entity.Estado;
import com.ridebot01.entity.Cliente;
import com.ridebot01.dao.ViajeFacade;
import com.ridebot01.dao.CorreoFacade;
import com.ridebot01.dao.ClienteFacade;
import com.ridebot01.dao.TipoVehiculoFacade;

public class SolicitudViaje {

	private CorreoFacade correoDAO;
	private Cliente cli;

	public SolicitudViaje() {
		correoDAO = new CorreoFacade();
	}

	// Accediendo a correos de solicitud de viaje
	public void atiendeSolicitud() {
		try {
			// Solicitudes de viaje nuevas
			List<Correo> solicitudes = correoDAO.getUnread("1");

			if (solicitudes.isEmpty() == false) {
				for (Correo co : solicitudes) {
					String[] data = co.getCuerpo().split("\\|");
					// Dando formato a coordenadas
					data[2] = data[2].trim().replace("Lat: ", "");
					data[2] = data[2].replace("Log: ", "");
					data[3] = data[3].trim().replace("Lat: ", "");
					data[3] = data[3].replace("Log: ", "");

					verificaCliente(data); // Verifica existencia de cliente
					registraViaje(data); // Ingresa viaje a DB
					co.setEstado(1); // Marca solicitud como atendida
					correoDAO.edit(co); // Actualizando registro
					System.out.println("SOLICITUD: " + co.getCuerpo() + " ATENDIDA.");
					System.out.println("DATOS GUARDADOS CON EX�TO.");
				}
			}
			
			System.out.println("");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void verificaCliente(String[] datos) {
		ClienteFacade clienteDAO = new ClienteFacade();
		cli = clienteDAO.findByPhonenumber(datos[1]);
		
		// Si el cliente no existe en la BD, se registra con datos recibidos
		if (cli == null) {
			cli = new Cliente();
			cli.setNombre(datos[0]);
			cli.setApellido("");
			cli.setDui("");
			cli.setTelefono(datos[1]);
			cli.setEmail(datos[7]);
			clienteDAO.create(cli);
		} else {
			// Si el cliente existe, se actualizan datos
			cli.setNombre(datos[0]);
			cli.setEmail(datos[7]);
			clienteDAO.edit(cli);
		}
	}

	private void registraViaje(String[] datos) {
		ViajeFacade viajeDAO = new ViajeFacade();
		TipoVehiculoFacade tipoDAO = new TipoVehiculoFacade();

		// Busca id del tipo de veh�culo solicitado
		int tipoCarro = tipoDAO.getIdTypeCar(datos[6]).getIdTipoVehiculo();

		// Creando viaje nuevo con los datos de la solicitud
		Viaje viaje = new Viaje();
		viaje.setFecha(new Date());
		viaje.setHoraInicio(new Time(new Date().getTime()));
		viaje.setEstadoBean(new Estado(1));
		viaje.setOrigen(datos[2]);
		viaje.setDestino(datos[3]);
		viaje.setCliente(cli);
		viaje.setPrecio(new BigDecimal(datos[4]));
		viaje.setIdTipoVehiculo(tipoCarro);
		viaje.setDetalleViajes(null);
		viajeDAO.create(viaje); // Registrando registro en DB
	}
}