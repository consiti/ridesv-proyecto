CREATE DATABASE  IF NOT EXISTS `ride` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ride`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: ride
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cliente` (
  `id_cliente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `dui` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `id_municipio` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_cliente`),
  UNIQUE KEY `telefono_UNIQUE` (`telefono`),
  KEY `fk_cliente_municipio1_idx` (`id_municipio`),
  CONSTRAINT `fk_cliente_municipio1` FOREIGN KEY (`id_municipio`) REFERENCES `municipio` (`id_municipio`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conductor`
--

DROP TABLE IF EXISTS `conductor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `conductor` (
  `id_conductor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `dui` varchar(45) NOT NULL,
  `num_licencia` varchar(45) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `id_municipio` int(10) unsigned NOT NULL,
  `id_vehiculo` int(10) unsigned NOT NULL,
  `email` varchar(45) NOT NULL,
  `fotografia` longblob,
  `telefono` varchar(9) NOT NULL,
  PRIMARY KEY (`id_conductor`),
  UNIQUE KEY `dui_UNIQUE` (`dui`),
  UNIQUE KEY `telefono_UNIQUE` (`telefono`),
  KEY `fk_conductor_municipio1_idx` (`id_municipio`),
  KEY `fk_conductor_vehiculo1_idx` (`id_vehiculo`),
  CONSTRAINT `fk_conductor_municipio1` FOREIGN KEY (`id_municipio`) REFERENCES `municipio` (`id_municipio`),
  CONSTRAINT `fk_conductor_vehiculo1` FOREIGN KEY (`id_vehiculo`) REFERENCES `vehiculo` (`id_vehiculo`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tr_conductor_posicion` AFTER INSERT ON `conductor` FOR EACH ROW BEGIN
	INSERT INTO conductor_posicion (id_posicion, ultima_posicion, estado, hora) 
    VALUES (NEW.id_conductor, 'XXXXXXX', 0, '2000-5-6');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `conductor_posicion`
--

DROP TABLE IF EXISTS `conductor_posicion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `conductor_posicion` (
  `id_posicion` int(11) NOT NULL AUTO_INCREMENT,
  `ultima_posicion` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL,
  `hora` datetime NOT NULL,
  PRIMARY KEY (`id_posicion`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tr_registro_posicion` AFTER UPDATE ON `conductor_posicion` FOR EACH ROW BEGIN
	
    DECLARE phone VARCHAR(10);
    SET phone = (SELECT telefono FROM conductor WHERE id_conductor = NEW.id_posicion);
    
    INSERT INTO conductor_posicion_historial (`posicion`, `estado`, `telefono`, `id_conductor`) 
    VALUES (NEW.ultima_posicion, NEW.estado, phone, NEW.id_posicion);
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `conductor_posicion_historial`
--

DROP TABLE IF EXISTS `conductor_posicion_historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `conductor_posicion_historial` (
  `id_historial` int(11) NOT NULL AUTO_INCREMENT,
  `posicion` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telefono` varchar(10) NOT NULL,
  `id_conductor` int(11) NOT NULL,
  PRIMARY KEY (`id_historial`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `correos`
--

DROP TABLE IF EXISTS `correos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `correos` (
  `idcorreos` int(11) NOT NULL AUTO_INCREMENT,
  `asunto` varchar(45) NOT NULL,
  `cuerpo` text NOT NULL,
  `estado` int(11) NOT NULL,
  `bot` varchar(10) NOT NULL,
  PRIMARY KEY (`idcorreos`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `departamento` (
  `id_departamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `departamento_nombre` varchar(45) NOT NULL,
  `iso_code` varchar(5) NOT NULL,
  `id_zona` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_departamento`),
  KEY `zona_foreing_key_idx` (`id_zona`),
  CONSTRAINT `zona_foreing_key` FOREIGN KEY (`id_zona`) REFERENCES `zona` (`id_zona`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` VALUES (1,'Ahuachapán','SV-AH',1),(2,'Santa Ana','SV-SA',1),(3,'Sonsonate','SV-SO',1),(4,'La Libertad','SV-LI',2),(5,'Chalatenango','SV-CH',2),(6,'San Salvador','SV-SS',2),(7,'Cuscatlán','SV-CU',3),(8,'La Paz','SV-PA',3),(9,'Cabañas','SV-CA',3),(10,'San Vicente','SV-SV',3),(11,'Usulutan','SV-US',4),(12,'Morazán','SV-MO',4),(13,'San Miguel','SV-SM',4),(14,'La Unión','SV-UN',4);
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;
--
-- Table structure for table `detalle_viaje`
--

DROP TABLE IF EXISTS `detalle_viaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `detalle_viaje` (
  `id_detalle_viaje` int(11) NOT NULL AUTO_INCREMENT,
  `id_viaje` int(10) unsigned NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id_detalle_viaje`),
  KEY `fk_detalle_viaje_viaje1_idx` (`id_viaje`),
  CONSTRAINT `fk_detalle_viaje_viaje1` FOREIGN KEY (`id_viaje`) REFERENCES `viaje` (`id_viaje`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_historial`
--

DROP TABLE IF EXISTS `error_historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `error_historial` (
  `id_error` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora` datetime DEFAULT CURRENT_TIMESTAMP,
  `descripcion_error` text NOT NULL,
  `metodo` varchar(45) NOT NULL,
  PRIMARY KEY (`id_error`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `estado` (
  `id_estado` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estado_nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Solicitado'),(2,'Reserva'),(3,'Cancelado'),(4,'En curso'),(5,'Completo'),(6,'En proceso');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipio`
--

DROP TABLE IF EXISTS `municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `municipio` (
  `id_municipio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `municipio_nombre` varchar(45) NOT NULL,
  `departamento` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_municipio`),
  KEY `fk_municipio_departamento1_idx` (`departamento`),
  CONSTRAINT `fk_municipio_departamento1` FOREIGN KEY (`departamento`) REFERENCES `departamento` (`id_departamento`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipio`
--

LOCK TABLES `municipio` WRITE;
/*!40000 ALTER TABLE `municipio` DISABLE KEYS */;
INSERT INTO `municipio` VALUES (1,'Ahuachapán',1),(2,'Apaneca',1),(3,'Atiquizaya',1),(4,'Concepción de Ataco',1),(5,'El Refugio',1),(6,'Guaymango',1),(7,'Jujutla',1),(8,'San Francisco Menéndez',1),(9,'San Lorenzo',1),(10,'San Pedro Puxtla',1),(11,'Tacuba',1),(12,'Turín',1),(13,'Cinquera',2),(14,'Dolores',2),(15,'Guacotecti',2),(16,'Ilobasco',2),(17,'Jutiapa',2),(18,'San isidro',2),(19,'Sensuntepeque',2),(20,'Tejutepeque',2),(21,'Victoria',2),(22,'Citalá',3),(23,'Comalapa',3),(24,'Concepción Quezaltepeque',3),(25,'Dulce Nombre de María',3),(26,'El Carrizal',3),(27,'El Paraíso',3),(28,'La Laguna',3),(29,'La Palma',3),(30,'La Reina',3),(31,'Las Vueltas',3),(32,'Nombre de Jesús',3),(33,'Nueva Concepción',3),(34,'Nueva Trinidad',3),(35,'Ojos de Agua',3),(36,'Potonico',3),(37,'San Antonio de la Cruz',3),(38,'San Antonio Los Ranchosl',3),(39,'San Fernando',3),(40,'San Francisco Lempa',3),(41,'San Francisco Morazán',3),(42,'San Ingacio',3),(43,'San Isidro Labrador',3),(44,'San José Cancasque',3),(45,'San José La Flores',3),(46,'San Luis del Carmen ',3),(47,'San Miguel de Mercedes',3),(48,'San Rfael ',3),(49,'Santa Rita',3),(50,'Tejutla',3),(51,'Candelaria',4),(52,'Cojutepeque',4),(53,'El Carmen',4),(54,'El Rosario',4),(55,'Monte San Juan',4),(56,'Oratorio de Concepción',4),(57,'San Bartolomé Perulapía',4),(58,'San Cristóbal',4),(59,'San JoséGuayagual',4),(60,'San Pedro Perulapán',4),(61,'San Rafael Cedros',4),(62,'San Ramón',4),(63,'Santa Cruz Analquito',4),(64,'Santa Cruz Michapa ',4),(65,'Suchitoto',4),(66,'Tenancingo',4),(67,'Arambala',5),(68,'Cacaopera',5),(69,'Chilanga',5),(70,'Corinto',5),(71,'Delicias de Concepción',5),(72,'El Divisadero',5),(73,'EL Rosario',5),(74,'Gualococti',5),(75,'Guatajiagua',5),(76,'Joateca',5),(77,'Jocoatique',5),(78,'Jocoro',5),(79,'Lolotiquillo',5),(80,'Meanguera',5),(81,'Osicala',5),(82,'Perquín',5),(83,'San Carlos',5),(84,'San Fernado',5),(85,'San Francisco Gotera',5),(86,'San Isidro',5),(87,'San Simón',5),(88,'Sensembra',5),(89,'Sociedad',5),(90,'Torola',5),(91,'Yamabal',5),(92,'Yoloaiquín',5),(93,'Antiguo Cuscatlán',6),(94,'Chiltiupán',6),(95,'Ciudad Arce',6),(96,'Colón',6),(97,'Comasagua',6),(98,'Huizúcar',6),(99,'Jayaque',6),(100,'Jicalapa',6),(101,'La Libertad',6),(102,'Santa Tecla',6),(103,'Nuevo Cuscatlán',6),(104,'San Juan Opico',6),(105,'Quezaltepeque',6),(106,'Sacacoyo',6),(107,'San José Villanueva',6),(108,'San Matías',6),(109,'San Pablo Tacachico',6),(110,'Talnique',6),(111,'Tamanique',6),(112,'Teotepeque',6),(113,'Tepecoyo',6),(114,'Zaragoza',6),(115,'Cuyultitán',7),(116,'El Rosario de la Paz',7),(117,'Jerusalén',7),(118,'Mercedes La Ceiba',7),(119,'Olocuilta',7),(120,'Paraíso de Osorio',7),(121,'San Antonio Masahuat',7),(122,'San Emigndio',7),(123,'San Francisco Chinameca',7),(124,'San Juan Nonualco',7),(125,'San Juan Talpa',7),(126,'San Juan Tepezontes',7),(127,'San Luis La Herradura',7),(128,'San Luis Talpa',7),(129,'San Miguel Tepezontes',7),(130,'San PEDRO nONUALCO',7),(131,'San Rafael Obrajuelo',7),(132,'Santa María Ostuma',7),(133,'Santiago Nonualco',7),(134,'Tapalhuaca',7),(135,'Zacatecoluca',7),(136,'Anamorós',8),(137,'Bolívar',8),(138,'Concepción de Oriente',8),(139,'Conchagua',8),(140,'El Carmen',8),(141,'El Sauce',8),(142,'Intipucá',8),(143,'La Unión',8),(144,'Lilisque',8),(145,'Meanguera del Golfo',8),(146,'Nueva Esparta',8),(147,'Polorós',8),(148,'San Alejo',8),(149,'San José',8),(150,'Santa Rosa De Lima',8),(151,'Yayantique',8),(152,'Yucuaiquín',8),(153,'Carolina',9),(154,'Chapeltique',9),(155,'Chinameca',9),(156,'Chirilagua',9),(157,'Ciudad Barrios',9),(158,'Comacarán',9),(159,'El Tránsito',9),(160,'Lolotique',9),(161,'Moncagua',9),(162,'Nueva Guadalupe',9),(163,'Nuevo Edén de San Juan',9),(164,'Quelepa',9),(165,'San Antonio del Mosco',9),(166,'San Gerardo ',9),(167,'San Jorge',9),(168,'San Luis de la Reina',9),(169,'San Miguel',9),(170,'San Rafael Oriente',9),(171,'Sesori',9),(172,'Uluazapa',9),(173,'Aguilares',10),(174,'Apopa',10),(175,'Ayutuxtepeque',10),(176,'Delgado',10),(177,'Cuscatancingo',10),(178,'El Paisnal',10),(179,'Guazapa',10),(180,'Ilopango',10),(181,'Mejicanos',10),(182,'Nejapa',10),(183,'Panchimalco',10),(184,'Rosario de Mora',10),(185,'San Marcos',10),(186,'San Martín',10),(187,'San Salvador',10),(188,'Santiago Texacuangos',10),(189,'Santo TOmas',10),(190,'Soyapango',10),(191,'Tonacatepeque',10),(192,'Candelaria de la Frontera ',12),(193,'Chalchuapa',12),(194,'Coatepeque',12),(195,'El Congo',12),(196,'El Porvenir',12),(197,'Masahuat',12),(198,'Metapán',12),(199,'San Antonio Pajonal',12),(200,'San Sebastián Salitrillo',12),(201,'Santa Ana',12),(202,'Santa Rosa Guachipilín',12),(203,'Santiago de la Frontera',12),(204,'Texistepeque',12),(205,'Acajutla',13),(206,'Armenia',13),(207,'Caluco',13),(208,'Cuisnahuat',13),(209,'Izalco',13),(210,'Juayúa',13),(211,'Nahuizalco',13),(212,'Nahulingo',13),(213,'Salcoatitán',13),(214,'San Antonio del Monte',13),(215,'San Julián ',13),(216,'Santa Catarina Masahuat',13),(217,'Santa Isabel Ishuatán',13),(218,'Santo Domingo de Guzmán ',13),(219,'Sonsonate',13),(220,'Sonzacate',13),(221,'Alegría',14),(222,'Berlín',14),(223,'Califormia',14),(224,'Concepción  Batres',14),(225,'El Triunfo',14),(226,'Ereguayaquín',14),(227,'Estanzuelas',14),(228,'Jiquilisco',14),(229,'Juacuapa',14),(230,'Jucuarán',14),(231,'Mercedes Umaña',14),(232,'Nueva Granada',14),(233,'Ozatlán',14),(234,'Puerto El Triunfo',14),(235,'San Agustín',14),(236,'San Buenaventura',14),(237,'San Dionisio',14),(238,'San Francisco Javier',14),(239,'Santa Elena',14),(240,'Santa María',14),(241,'Santiago de María',14),(242,'Tecapán',14),(243,'Usulután',14),(244,'Agua Caliente',3),(245,'Arcatao',3),(246,'Chalatenango',3),(247,'Apastepeque',11),(248,'Guadalupe',11),(249,'San Cayetano Istepeque',11),(250,'San Esteban Catarina',11),(251,'San Ildefonso',11),(252,'San Lorenzo',11),(253,'San Sebastián',11),(254,'San Vicente',11),(255,'Santa Clara',11),(256,'Santo Domingo',11),(257,'Tecoluca',11),(258,'Tepetitán',11),(259,'Verapaz',11);
/*!40000 ALTER TABLE `municipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_vehiculo`
--

DROP TABLE IF EXISTS `tipo_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipo_vehiculo` (
  `id_tipo_vehiculo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_vehiculo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_vehiculo`
--

LOCK TABLES `tipo_vehiculo` WRITE;
/*!40000 ALTER TABLE `tipo_vehiculo` DISABLE KEYS */;
INSERT INTO `tipo_vehiculo` VALUES (1,'Automóvil'),(2,'Pick Up'),(3,'Microbus');
/*!40000 ALTER TABLE `tipo_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracking`
--

DROP TABLE IF EXISTS `tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tracking` (
  `idtracking` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_conductor` int(11) NOT NULL,
  `id_viaje` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`idtracking`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `vehiculo` (
  `id_vehiculo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `placa` varchar(45) NOT NULL,
  `marca` varchar(45) NOT NULL,
  `modelo` varchar(45) NOT NULL,
  `color` varchar(45) NOT NULL,
  `fotografia` longblob,
  `fecha` varchar(45) NOT NULL,
  `id_tipo_vehiculo` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_vehiculo`),
  UNIQUE KEY `placa_UNIQUE` (`placa`),
  KEY `FK_VEHICULO_TIPO_idx` (`id_tipo_vehiculo`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `viaje`
--

DROP TABLE IF EXISTS `viaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `viaje` (
  `id_viaje` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `estado` int(10) unsigned DEFAULT NULL,
  `origen` varchar(200) DEFAULT NULL,
  `destino` varchar(200) DEFAULT NULL,
  `id_cliente` int(10) unsigned DEFAULT NULL,
  `id_conductor` int(10) unsigned DEFAULT NULL,
  `precio` decimal(10,2) unsigned DEFAULT NULL,
  `id_tipo_vehiculo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_viaje`),
  KEY `fk_viaje_estado1_idx` (`estado`),
  KEY `fk_viaje_cliente1_idx` (`id_cliente`),
  KEY `fk_viaje_conductor1_idx` (`id_conductor`),
  CONSTRAINT `fk_viaje_cliente1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  CONSTRAINT `fk_viaje_conductor1` FOREIGN KEY (`id_conductor`) REFERENCES `conductor` (`id_conductor`),
  CONSTRAINT `fk_viaje_estado1` FOREIGN KEY (`estado`) REFERENCES `estado` (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zona`
--

DROP TABLE IF EXISTS `zona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `zona` (
  `id_zona` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) NOT NULL,
  PRIMARY KEY (`id_zona`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zona`
--

LOCK TABLES `zona` WRITE;
/*!40000 ALTER TABLE `zona` DISABLE KEYS */;
INSERT INTO `zona` VALUES (1,'Occidental'),(2,'Central'),(3,'Paracentral'),(4,'Oriental');
/*!40000 ALTER TABLE `zona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'ride'
--

--
-- Dumping routines for database 'ride'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-09 17:57:34
