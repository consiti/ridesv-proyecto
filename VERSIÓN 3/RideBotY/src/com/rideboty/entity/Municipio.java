package com.rideboty.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the municipio database table.
 * 
 */
@Entity
@NamedQuery(name="Municipio.findAll", query="SELECT m FROM Municipio m")
public class Municipio implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idMunicipio;
	private String municipioNombre;
	private List<Cliente> clientes;
	private List<Conductor> conductors;
	private Departamento departamentoBean;

	public Municipio() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_municipio")
	public int getIdMunicipio() {
		return this.idMunicipio;
	}

	public void setIdMunicipio(int idMunicipio) {
		this.idMunicipio = idMunicipio;
	}


	@Column(name="municipio_nombre")
	public String getMunicipioNombre() {
		return this.municipioNombre;
	}

	public void setMunicipioNombre(String municipioNombre) {
		this.municipioNombre = municipioNombre;
	}


	//bi-directional many-to-one association to Cliente
	@OneToMany(mappedBy="municipio")
	public List<Cliente> getClientes() {
		return this.clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public Cliente addCliente(Cliente cliente) {
		getClientes().add(cliente);
		cliente.setMunicipio(this);

		return cliente;
	}

	public Cliente removeCliente(Cliente cliente) {
		getClientes().remove(cliente);
		cliente.setMunicipio(null);

		return cliente;
	}


	//bi-directional many-to-one association to Conductor
	@OneToMany(mappedBy="municipio")
	public List<Conductor> getConductors() {
		return this.conductors;
	}

	public void setConductors(List<Conductor> conductors) {
		this.conductors = conductors;
	}

	public Conductor addConductor(Conductor conductor) {
		getConductors().add(conductor);
		conductor.setMunicipio(this);

		return conductor;
	}

	public Conductor removeConductor(Conductor conductor) {
		getConductors().remove(conductor);
		conductor.setMunicipio(null);

		return conductor;
	}


	//bi-directional many-to-one association to Departamento
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="departamento")
	public Departamento getDepartamentoBean() {
		return this.departamentoBean;
	}

	public void setDepartamentoBean(Departamento departamentoBean) {
		this.departamentoBean = departamentoBean;
	}

}