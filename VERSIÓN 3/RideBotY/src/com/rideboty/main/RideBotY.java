package com.rideboty.main;

import java.util.Timer;
import java.util.TimerTask;

public class RideBotY {

	public static void main(String[] args) {
		final Timer timer = new Timer();
        //Se repite la lectura de los mensajes de inbox cada 5 segs.
        TimerTask task = new TimerTask() {
            int conta = 0;
            ViajeRechazado mail = new ViajeRechazado();
            @Override public void run() {
                conta++;
                System.out.println("Iteración " + conta);
                mail.atiendeRechazo();
            }
        };
        timer.schedule(task, 0, 5000);
	}
}