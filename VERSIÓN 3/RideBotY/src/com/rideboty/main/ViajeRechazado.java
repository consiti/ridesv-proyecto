package com.rideboty.main;

import java.util.ArrayList;
import java.util.List;
import com.rideboty.dao.*;
import com.rideboty.entity.Conductor;
import com.rideboty.entity.ConductorPosicion;
import com.rideboty.entity.Correo;
import com.rideboty.entity.Estado;
import com.rideboty.entity.Tracking;
import com.rideboty.entity.Viaje;

public class ViajeRechazado {
    private List<Correo> correos;
    private CorreoFacade CorreoDAO;
	
    public ViajeRechazado(){
    	correos = new ArrayList<Correo>();
    	CorreoDAO = new CorreoFacade();
    }

    //Se lee contenido de correo y se registra de solicitud de viaje
    public void atiendeRechazo() {
        try{//Busca los correos correspondientes al bot Y
        	correos = CorreoDAO.getUnread("Y");
        	if(!correos.isEmpty()) {
	        	for(Correo c : correos) {
	        		String[] data = c.getAsunto().split("-");
	        		c.setEstado(1);			//Marcando correo como leido
	        		CorreoDAO.edit(c);
	        		//PRIMERO VIENE TELEFONO Y LUEGO VIAJE
	        		actualizaViaje(data[3]);
	        		actualizaEstados(data[3], data[2]);
	        	}
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void actualizaViaje(String idViaje){
    	//Actualizando estado de viaje - No ha sido aceptado por driver
    	//Queda viaje como "disponible" para asignar a otro driver
		ViajeFacade vDAO = new ViajeFacade();
		Viaje v = vDAO.find(Integer.valueOf(idViaje));
		v.setEstadoBean(new Estado(1));
		vDAO.edit(v);
		System.out.println("Viaje fue rechazado, pasa a estado 'Solicitado' nuevamente");
    }
    
    public void actualizaEstados(String idViaje, String telefono){
        ConductorFacade cDAO = new ConductorFacade();
        Conductor conductor = cDAO.findByNumber(telefono);
        
        //Se actualiza historial, aclarando que conductor rechaza hacer el viaje
        Tracking track = new Tracking();
        TrackingFacade trackDAO = new TrackingFacade();
        track = trackDAO.findByDetails(Integer.valueOf(idViaje), conductor.getIdConductor());
        track.setEstado(3);
        trackDAO.edit(track);
        System.out.println("Actualizando tracking, conductor " + conductor.getNombre() + " " + conductor.getApellido() + " rechaza el viaje");

        //Estado de conductor pasa a activo, se le puede asignar otro viaje
        PosicionFacade cpDAO = new PosicionFacade();
        ConductorPosicion cp = cpDAO.find(conductor.getIdConductor());
        cp.setEstado(1);
        cpDAO.edit(cp);
        System.out.println("Conductor pasa a estado activo");
    }
}