package com.rideboty.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import com.rideboty.entity.Viaje;
import com.rideboty.util.JpaUtil;

public class ViajeFacade extends AbstractFacade<Viaje> implements Serializable{
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public ViajeFacade() {
		super(Viaje.class);
		getEntityManager();
	}
	
	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
}