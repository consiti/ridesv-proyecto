package com.rideboty.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.rideboty.entity.Tracking;
import com.rideboty.util.JpaUtil;

public class TrackingFacade extends AbstractFacade<Tracking> implements Serializable{
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public TrackingFacade() {
		super(Tracking.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager(); 
		}
		return em;
	}
	
	public Tracking findByDetails(int idViaje, int idCond) {
		em = JpaUtil.getEntityManager().createEntityManager();
        Query q = em.createNamedQuery("Tracking.findByDetails");
        //Pasando parámetros a consulta predefinida
        q.setParameter("idViaje", idViaje);
        q.setParameter("idCond", idCond);
        return (Tracking) q.getSingleResult();
	}
}