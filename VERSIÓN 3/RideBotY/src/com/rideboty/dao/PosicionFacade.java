package com.rideboty.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import com.rideboty.entity.ConductorPosicion;
import com.rideboty.util.JpaUtil;

public class PosicionFacade extends AbstractFacade<ConductorPosicion> implements Serializable{

	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public PosicionFacade() {
		super(ConductorPosicion.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
}
