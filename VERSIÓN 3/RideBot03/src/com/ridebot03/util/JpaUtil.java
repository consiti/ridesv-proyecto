package com.ridebot03.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	private final static String PERSISTENCE_UNIT = "RideBot03";
	private static EntityManagerFactory factory;
	
	public static EntityManagerFactory getEntityManager() {
		if(factory == null) {
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		}
		
		return factory;
	}
}