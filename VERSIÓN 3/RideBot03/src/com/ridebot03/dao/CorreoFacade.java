package com.ridebot03.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ridebot03.entity.Correo;
import com.ridebot03.util.JpaUtil;

public class CorreoFacade extends AbstractFacade<Correo> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public CorreoFacade() {
		super(Correo.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
	
	@SuppressWarnings("unchecked")
	public List<Correo> getUnread(String bot){
		em = getEntityManager();
		Query q = em.createNamedQuery("Correo.findUnread").setParameter("bot", bot);
		return q.getResultList();
	}
}