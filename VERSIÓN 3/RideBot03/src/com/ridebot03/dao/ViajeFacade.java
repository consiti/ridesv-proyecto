package com.ridebot03.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;
import com.ridebot03.entity.Viaje;
import com.ridebot03.util.JpaUtil;

public class ViajeFacade extends AbstractFacade<Viaje> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public ViajeFacade() {
		super(Viaje.class);
		getEntityManager();
	}
	
	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
}