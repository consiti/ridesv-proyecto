package com.ridebot03.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.ridebot03.entity.ConductorPosicion;
import com.ridebot03.util.JpaUtil;

public class PosicionFacade extends AbstractFacade<ConductorPosicion> implements Serializable {
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public PosicionFacade() {
		super(ConductorPosicion.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
	
	public ConductorPosicion findPosicionConductor(int id){
		em = getEntityManager();
		Query q = em.createNamedQuery("ConductorPosicion.findDriverLocation").setParameter("idCond", id);
		return (ConductorPosicion) q.getSingleResult();
	}
}