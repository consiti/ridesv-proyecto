package com.ridebot03.main;

import java.util.Timer;
import java.util.TimerTask;

import com.ridebot03.main.ViajeAceptado;

public class RideBot03 {
	
	public static void main(String[] args) {
		
		final Timer timer = new Timer();
		// Se repite la lectura de los mensajes de inbox cada 5 segs.
		TimerTask task = new TimerTask() {
			int conta = 0;
			ViajeAceptado mail = new ViajeAceptado();

			@Override
			public void run() {
				conta++;
				System.out.println("Iteración " + conta);
				mail.atiendeConfirmacion();
			}
		};
		timer.schedule(task, 0, 5000);
	}
}