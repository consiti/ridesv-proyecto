package com.ridebot03.main;

import com.ridebot03.dao.ConductorFacade;
import com.ridebot03.entity.Conductor;
import com.ridebot03.util.JpaUtil;

public class Test {

	public static void main(String[] args) {
		
		ConductorFacade cF = new ConductorFacade();
		
//		if (JpaUtil.getEntityManager() != null) {
//			System.out.println("Exito: conexi�n a la persistencia.");
//		} else {
//			System.err.println("Error: no se pudo conectar a la persistencia.");
//		}
		
		Conductor conductor = cF.findByPhone("22325947");
		
		System.out.println(conductor.getNombre() + " " + conductor.getApellido() + " " + conductor.getDui() + " " + conductor.getTelefono());

	}

}
