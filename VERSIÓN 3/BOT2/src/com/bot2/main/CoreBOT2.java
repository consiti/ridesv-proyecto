package com.bot2.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bot2.calcular.ConductorMasProximo;
import com.bot2.calcular.SolicitudViaje;
import com.bot2.dao.ConductorFacade;
import com.bot2.dao.ConductorPosicionFacade;
import com.bot2.dao.TrackingFacade;
import com.bot2.dao.ViajeFacade;
import com.bot2.email.EmailSolicitudViaje;
import com.bot2.entity.Conductor;
import com.bot2.entity.ConductorPosicion;
import com.bot2.entity.Estado;
import com.bot2.entity.Tracking;
import com.bot2.entity.Viaje;

public class CoreBOT2 {
	
	public void run() {
		
		/*LOGICA DE CONSULTAR LA BASE Y CALCULAR LA DISTANCIA MAS CORTA-------------------------------------------------------------*/
		
		//Consulta a la base de Datos
		ViajeFacade viajeFacade = new ViajeFacade();
		ConductorPosicionFacade conductorPosicionFacade = new ConductorPosicionFacade();
		ConductorFacade conductorFacade = new ConductorFacade();
		TrackingFacade trackingFacade = new TrackingFacade();
		
		Tracking tracking = new Tracking();
		
		if(viajeFacade.findAll().isEmpty() || viajeFacade.findAll().isEmpty()){//Validacion si esta vacia la tabla
			System.out.println("La tabla de <viaje> o la tabla de <ConductorPosicion> estan vacias");
		}else {

			//Busca las solicitudes del viaje su estado = Solicitado
			for(Viaje cv: viajeFacade.findAll()) {
				if(cv.getEstadoBean().getIdEstado() == 1) {
					
					//Crea la lista para ser calculada
					List<ConductorPosicion> listaConductorPosicion = new ArrayList<ConductorPosicion>();
					
					//Busca el conductor cuyo estado este disponible y su veiculo sea el indicado
					for(ConductorPosicion cc: conductorPosicionFacade.findAll()) {
						Conductor conductor = conductorFacade.find(cc.getIdPosicion());
						if(cc.getEstado() == 1 && conductor.getVehiculo().getIdTipoVehiculo() == cv.getIdTipoVehiculo()) {
							
							//Filtro de personas que rechazaron la solicitud de viaje
							List<Tracking> listaTracking = trackingFacade.getConductoresByIdviajeAndIdconductorAndEstado(cv.getIdViaje(), cc.getIdPosicion(), 3);
							if(listaTracking.size() == 0) {
								listaConductorPosicion.add(cc);
							}
							
						}
						
					}
					
					if(listaConductorPosicion.isEmpty()) {
						System.out.println("NO HAY CONDUCTORES DISPONIBLES PARA LA SOLICITUD DE VIAJE ID = "+cv.getIdViaje());
					}else {
						//Busca la ruta mas corta
						SolicitudViaje solicitudViaje = ConductorMasProximo.getConductor(cv, listaConductorPosicion);
						
						//-----------------------------CAMBIOS EN EL ESTADO DE LA BASE DE DATOS RIDE-----------------------------
					
						//Cambia el estado del viaje de "Solicitado" a "En proceso" 
						cv.setEstadoBean(new Estado(6));
						viajeFacade.edit(cv);
						
						//Cambia el estado del conductor de "activo" a "En proceso"
						ConductorPosicion cc = conductorPosicionFacade.find(solicitudViaje.getIdConductor());
						cc.setEstado(4);
						conductorPosicionFacade.edit(cc);
						
						//Realiza una insecion en la tabla tracking con el idConductor y el idViaje con el estado "En espera"
						tracking.setIdConductor(solicitudViaje.getIdConductor());
						tracking.setIdViaje(solicitudViaje.getIdViaje());
						tracking.setFecha(new Date());
						tracking.setEstado(1);
						trackingFacade.create(tracking);
						
						/*----------ENVIAR EL CORREO HACIA LOS CONDUCTORES CON LA PETICION DEL VIAJE--------------------*/
						
						EmailSolicitudViaje.sendEmail(solicitudViaje);
						
					}
					
				}
				
			}		
		
		}
		
	}

}
