package com.bot2.main;

import java.util.Timer;
import java.util.TimerTask;

public class MainBot2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		final Timer timer = new Timer();
        
        TimerTask task = new TimerTask() {
            int conta = 0;
            CoreBOT2 bot2 =  new CoreBOT2();
            @Override public void run() {
                conta++;
                System.out.println("BOT-2 Iteracion = " + conta);
                bot2.run(); 
                
                
            }
        };
        timer.schedule(task, 0, 5000);

	}

}
