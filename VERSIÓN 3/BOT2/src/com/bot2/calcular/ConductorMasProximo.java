package com.bot2.calcular;

import static java.lang.Math.sqrt;

import java.util.Arrays;
import java.util.List;

import com.bot2.entity.ConductorPosicion;
import com.bot2.entity.Viaje;

public class ConductorMasProximo {
	
	public static SolicitudViaje getConductor(Viaje viaje,List<ConductorPosicion> listaConductorPosicion) {
		
		//Variables
		/*
		 * Posicion Inicial x1 = Latitud y y1 = Longitud
		 * Posicion Final x2 = Latitud y y2 = Longitud
		 *
		 */
		double x1,x2,y1,y2,distancia;
		AregloConductores[] aregloConductores = new AregloConductores[listaConductorPosicion.size()];
		
		//Asignacion de variables Posicion Inicial
		String origenCliente = viaje.getOrigen();
		String[] origenClientePartes = origenCliente.split(",");
		
		x1 = Double.valueOf(origenClientePartes[0]);
		y1 = Double.valueOf(origenClientePartes[1]);		
		
		//Asignacion de variables Posicion Final
		for (int i = 0; i < listaConductorPosicion.size(); i++) {
			String origenConductor = listaConductorPosicion.get(i).getUltimaPosicion();
			String[] origenConductorPartes = origenConductor.split(",");
			
			x2 =  Double.valueOf(origenConductorPartes[0]);
			y2 =  Double.valueOf(origenConductorPartes[1]);
			
			//Calcula la distancia entre las posiciones
			distancia = sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
			
			//llena el arglo con las posiciones de todos los conductores
			aregloConductores[i] = new AregloConductores(listaConductorPosicion.get(i).getIdPosicion(),distancia);

		}
		
		//Ordena de menos a mayor las distancias de los conductores 
		Arrays.sort(aregloConductores);
		
		SolicitudViaje solicitudViaje = new SolicitudViaje();
		solicitudViaje.setIdViaje(viaje.getIdViaje());
		solicitudViaje.setIdConductor(aregloConductores[0].getId());
		
		//Imprime en consola los datos del viaje
		
		System.out.println("----------------SOLICITUDES DE VIAJE----------------");
		System.out.println("ID-VIAJE = "+viaje.getIdViaje());
		System.out.println("ORIGEN-VIAJE = "+viaje.getOrigen());
		System.out.println("POSIBLES CONDUCTORES");
		for (int i = 0; i < aregloConductores.length; i++) {
			System.out.println("ID-CONDUCTOR = "+aregloConductores[i].getId());
			System.out.println("DISTANCIA = "+aregloConductores[i].getDistancia());
		}
		
		
		return solicitudViaje;
		
	}

}
