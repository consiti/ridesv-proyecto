package com.bot2.calcular;


public class AregloConductores implements Comparable<AregloConductores>{
	
	private int id;
    private double distancia;
    
	public AregloConductores() {
		super();
	}

	public AregloConductores(int id, double distancia) {
		super();
		this.id = id;
		this.distancia = distancia;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	//Sobre escribe el metodo para ordenar
    @Override
    public int compareTo(AregloConductores o) {
        if (distancia < o.distancia) {
            return -1;
        }
        if (distancia > o.distancia) {
            return 1;
        }
        return 0;
    }

}
