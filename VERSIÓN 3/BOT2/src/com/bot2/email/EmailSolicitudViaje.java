package com.bot2.email;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.bot2.dao.*;
import com.bot2.entity.*;

import eu.bitm.NominatimReverseGeocoding.NominatimReverseGeocodingJAPI;

import com.bot2.calcular.SolicitudViaje;

public class EmailSolicitudViaje {
	
	public static void sendEmail(SolicitudViaje solicitudViaje) {
		
		String asuntoCorreo = "ride-solicitud";
		String cuerpoCorreo = "";
		
		//Consulta ala Base de datos de la informacion a enviar
		ViajeFacade viajeFacade = new ViajeFacade();
		Viaje viaje = viajeFacade.find(solicitudViaje.getIdViaje());
		ClienteFacade clienteFacade = new ClienteFacade();
		Cliente cliente = clienteFacade.find(viaje.getCliente().getIdCliente());
		ConductorFacade conductorFacade = new ConductorFacade();
		Conductor conductor = conductorFacade.find(solicitudViaje.getIdConductor());
		
		//Prepara las coordenadas de latitud y longitud para transformarlas en direccion
		String origenCliente = viaje.getOrigen();
		String destinoCliente = viaje.getDestino();
		String[] origenClientePartes = origenCliente.split(",");
		String[] destinoClientePartes = destinoCliente.split(",");
		NominatimReverseGeocodingJAPI reverseGeocoding = new NominatimReverseGeocodingJAPI();
		
		//Armando el correo con los campos necesarios
		asuntoCorreo = asuntoCorreo+"-"+conductor.getTelefono()+"-"+viaje.getIdViaje();
		cuerpoCorreo = cuerpoCorreo+"-----------------SOLICITUD DE VIAJE-----------------\n";
		cuerpoCorreo = cuerpoCorreo+"PRECIO DEL VIAJE : "+viaje.getPrecio()+"\n";
		cuerpoCorreo = cuerpoCorreo+"CORREO DEL CLIENTE : "+cliente.getEmail()+"\n";
		cuerpoCorreo = cuerpoCorreo+"DIRECCION ORIGEN-CLIENTE : "+reverseGeocoding.getAdress(Double.valueOf(origenClientePartes[0]), Double.valueOf(origenClientePartes[1]))+"\n";
		cuerpoCorreo = cuerpoCorreo+"DIRECCION DESTINO-CLIENTE : "+reverseGeocoding.getAdress(Double.valueOf(destinoClientePartes[0]), Double.valueOf(destinoClientePartes[1]))+"\n\n";
		
		cuerpoCorreo = cuerpoCorreo+"-----------------PARA CONTESTAR EL CORREO-----------------\n";
		cuerpoCorreo = cuerpoCorreo+"Responda el correo con la palabra 'aceptar' o 'rechazar' si bien desea o no realizar el viaje ADVERTENCIA si usten no contesta el correo no se le podran asignar nuevos viajes posteriormente\n";
		
		//configuracion para enviar correo
	    final String username = "info.ridesv2019@gmail.com";
		final String password ="Consiti2018";
	
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
	
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			}
		);
	
		try {
	
		      // Define message
		      MimeMessage message = new MimeMessage(session);
		      message.setFrom(new InternetAddress(username));
		      message.setSubject(asuntoCorreo);
		      message.addRecipient(Message.RecipientType.TO,new InternetAddress(conductor.getEmail()));
		      message.setText(cuerpoCorreo);
		      
		      // Envia el mensaje
		      Transport.send(message);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
	}
         
}
