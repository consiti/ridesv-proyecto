package com.bot2.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.bot2.entity.ConductorPosicion;
import com.bot2.util.JpaUtil;

public class ConductorPosicionFacade extends AbstractFacade<ConductorPosicion> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public ConductorPosicionFacade() {
        super(ConductorPosicion.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }

}
