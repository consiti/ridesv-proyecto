package com.bot2.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.bot2.dao.AbstractFacade;
import com.bot2.entity.Tracking;
import com.bot2.util.JpaUtil;

public class TrackingFacade extends AbstractFacade<Tracking> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public TrackingFacade() {
        super(Tracking.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
    
    
    public List<Tracking> getConductoresByIdviajeAndIdconductorAndEstado(int idViaje,int idConductor,int estadoTracking){
		
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createQuery("SELECT c FROM Tracking c WHERE c.idConductor = :idConductor AND c.idViaje = :idViaje AND c.estado = :estado");
		query.setParameter("idViaje", idViaje);
		query.setParameter("idConductor", idConductor);
		query.setParameter("estado", estadoTracking);
		
		try {
			return (List<Tracking>)query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}
		
	}

}
