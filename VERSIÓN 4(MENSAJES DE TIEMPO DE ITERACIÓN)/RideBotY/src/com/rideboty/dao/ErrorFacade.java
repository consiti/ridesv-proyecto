package com.rideboty.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import com.rideboty.entity.ErrorHistorial;
import com.rideboty.util.JpaUtil;

public class ErrorFacade extends AbstractFacade<ErrorHistorial> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ErrorFacade() {
		super(ErrorHistorial.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
}