package com.rideboty.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		ViajeRechazado vNeg = new ViajeRechazado();
		ViajeSinRespuesta vNull = new ViajeSinRespuesta();

		// FECHA Y HORA DE INICIO
		DateFormat dateForma = new SimpleDateFormat("YYYY-MM-dd' 'HH:mm:ss");
		Date dateInitBot = new Date();
		System.out.println("BOT Y SE INICIA: " + dateForma.format(dateInitBot) + "\n");

		// Se ejecutan ambos hilos
		vNeg.start(); // Hilo para viajes rechazados
		vNull.start(); // Hilo para viajes sin respuesta
	}
}