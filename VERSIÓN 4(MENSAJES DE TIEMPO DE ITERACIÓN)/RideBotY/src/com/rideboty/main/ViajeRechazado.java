package com.rideboty.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.rideboty.dao.*;
import com.rideboty.entity.Conductor;
import com.rideboty.entity.ConductorPosicion;
import com.rideboty.entity.Correo;
import com.rideboty.entity.ErrorHistorial;
import com.rideboty.entity.Estado;
import com.rideboty.entity.Tracking;
import com.rideboty.entity.Viaje;

public class ViajeRechazado extends Thread {
	private List<Correo> correos;
	private CorreoFacade CorreoDAO;
	private ConductorFacade cDAO;
	private Conductor conductor;
	private int idViaje;

	public ViajeRechazado() {
		correos = new ArrayList<Correo>();
		CorreoDAO = new CorreoFacade();
		cDAO = new ConductorFacade();
	}

	@Override
	public void run() {
		final Timer timer = new Timer();

		DateFormat dateForma = new SimpleDateFormat("YYYY-MM-dd' 'HH:mm:ss");

		// Se repite la lectura de la tabla correo cada 5 segs.
		TimerTask task = new TimerTask() {
			int conta = 0;
			
			public void run() {
				conta++;
				Date dateInitCurrent = new Date();
				System.out.println("BOT-Y HILO_RECHAZO ITERACI�N " + conta);
				System.out.println("HILO_RECHAZO INICIO DE ITERACI�N: " + dateForma.format(dateInitCurrent));
				
				atiendeRechazo();
				
				Date dateFinalCurrent = new Date();
				System.out.println("HILO_RECHAZO FINAL DE ITERACI�N: " + dateForma.format(dateFinalCurrent));
				
				System.out.println("HILO_RECHAZO TIEMPO DE ITERACI�N: " + ((dateFinalCurrent.getTime() / 1000) - (dateInitCurrent.getTime() / 1000)) + "\n");
			}
		};
		
		timer.schedule(task, 0, 5000);
	}

	// Lee las solicitudes y las ingresa a la tabla viaje
	public void atiendeRechazo() {
		try {// Busca los correos correspondientes al bot Y
			correos = CorreoDAO.getUnread("Y");
			if (!correos.isEmpty()) {
				for (Correo c : correos) {
					String[] data = c.getAsunto().split("-");
					c.setEstado(1); // Marcando registro como le�do
					CorreoDAO.edit(c);
					idViaje = Integer.valueOf(data[3]);
					conductor = cDAO.findByNumber(data[2]);

					Tracking track = verificaTracking();
					if (track.getEstado() == 1) {
						actualizaEstados();
						actualizaViaje();
					} else {
						System.out.println("HILO_RECHAZO - Respuesta recibida despues del tiempo valido; se descarta");
					}
				}
			} else
				System.out.println("HILO_RECHAZO - Aun no hay rechazos de viajes");
		} catch (Exception e) {
			ErrorHistorial error = new ErrorHistorial();
			ErrorFacade errorDAO = new ErrorFacade();
			// Recopilando datos de errores surgidos en la ejecucion del BOT
			error.setFechaHora(new Date());
			error.setDescripcionError(
					"CAUSA: " + e.getCause() + " MENSAJE: " + e.getMessage() + " CLASS: " + e.getClass());
			error.setMetodo("BOT Y->ViajeRechazado->atiendeRechazo()");
			errorDAO.create(error); // Almacenando registro en BD
			e.printStackTrace();
		}
	}

	private Tracking verificaTracking() {
		TrackingFacade trackDAO = new TrackingFacade();
		return trackDAO.findByDetails(idViaje, conductor.getIdConductor());
	}

	private void actualizaViaje() {
		// Actualizando estado de viaje - No ha sido aceptado por driver
		// Queda viaje como "disponible" para asignar a otro driver
		ViajeFacade vDAO = new ViajeFacade();
		Viaje v = vDAO.find(idViaje);
		v.setEstadoBean(new Estado(1));
		vDAO.edit(v);
		System.out.println(
				"HILO_RECHAZO - Viaje " + idViaje + " fue rechazado por conductor, pasa a estado solicitado (1)");
	}

	private void actualizaEstados() {
		// Se actualiza historial, aclarando que conductor rechaza hacer el viaje
		Tracking track = new Tracking();
		TrackingFacade trackDAO = new TrackingFacade();
		track = trackDAO.findByDetails(idViaje, conductor.getIdConductor());
		track.setEstado(3);
		trackDAO.edit(track);
		System.out.println("HILO_RECHAZO - Tracking de viaje " + idViaje + " pasa a estado rechazado (3)");

		// Estado de conductor pasa a activo; se le puede asignar otro viaje
		PosicionFacade cpDAO = new PosicionFacade();
		ConductorPosicion cp = cpDAO.find(conductor.getIdConductor());
		cp.setEstado(1);
		cpDAO.edit(cp);
		System.out.println("HILO_RECHAZO - Conductor " + cp.getIdPosicion() + " pasa a estado activo (1)");
	}
}