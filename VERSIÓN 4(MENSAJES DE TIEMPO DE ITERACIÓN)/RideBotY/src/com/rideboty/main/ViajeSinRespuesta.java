package com.rideboty.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.rideboty.dao.ErrorFacade;
import com.rideboty.dao.PosicionFacade;
import com.rideboty.dao.TrackingFacade;
import com.rideboty.dao.ViajeFacade;
import com.rideboty.entity.ConductorPosicion;
import com.rideboty.entity.ErrorHistorial;
import com.rideboty.entity.Estado;
import com.rideboty.entity.Tracking;
import com.rideboty.entity.Viaje;

public class ViajeSinRespuesta extends Thread {
	List<Tracking> tracks;
	TrackingFacade trackDAO;

	public ViajeSinRespuesta() {
		tracks = new ArrayList<Tracking>();
		trackDAO = new TrackingFacade();
	}

	@Override
	public void run() {
		final Timer timer = new Timer();
		
		DateFormat dateForma = new SimpleDateFormat("YYYY-MM-dd' 'HH:mm:ss");

		// Se repite la lectura de la tabla correo cada 5 segs.
		TimerTask task = new TimerTask() {
			int conta = 0;
			
			public void run() {
				conta++;
				Date dateInitCurrent = new Date();
				System.out.println("BOT-Y HILO_SIN_RESPUESTA ITERACIÓN " + conta);
				System.out.println("HILO_SIN_RESPUESTA INICIO DE ITERACIÓN: " + dateForma.format(dateInitCurrent));
				
				atiendeSinRespuesta();
				
				Date dateFinalCurrent = new Date();
				System.out.println("HILO_SIN_RESPUESTA FINAL DE ITERACIÓN: " + dateForma.format(dateFinalCurrent));
				
				System.out.println("HILO_SIN_RESPUESTA TIEMPO DE ITERACIÓN: " + ((dateFinalCurrent.getTime() / 1000) - (dateInitCurrent.getTime() / 1000)) + "\n");
			}
		};
		
		timer.schedule(task, 0, 5000);
	}

	public void atiendeSinRespuesta() {
		try {
			tracks = trackDAO.findByState(1); // Busca viajes que esperan respuesta de conductor
			if (!tracks.isEmpty()) {
				Date fechaAct = new Date();
				for (Tracking t : tracks) {
					// Obtiene tiempo que tiene en espera la solicitud de viaje
					long dif = (fechaAct.getTime() - t.getFecha().getTime()) / 1000;

					if (dif > 300) { // Si no se ha obtenido respuesta en 5 mins (300 segs.) o mas...
						t.setEstado(4); // Se actualiza estado en tracking a no contestado (4)
						trackDAO.edit(t);
						System.out.println("HILO_SIN_RESPUESTA - Tracking de viaje " + t.getIdViaje()
								+ " pasa a estado no contestado (4)");
						// Se actualizan estados relacionados al viaje
						actualizaConductor(t.getIdConductor());
						actualizaViaje(t.getIdViaje());
					}
				}
			} else
				System.out.println("HILO_SIN_RESPUESTA - No hay viajes pendientes de respuesta");
		} catch (Exception e) {
			ErrorHistorial error = new ErrorHistorial();
			ErrorFacade errorDAO = new ErrorFacade();
			// Recopilando datos de errores surgidos en la ejecucion del BOT
			error.setFechaHora(new Date());
			error.setDescripcionError(e.getStackTrace().toString());
			error.setMetodo("BOT Y->ViajeSinRespuesta->atiendeSinRespuesta()");
			errorDAO.create(error); // Almacenando registro en BD
		}
	}

	private void actualizaViaje(int idViaje) {
		// Viaje pasa a estado inicial para que se asigne a otro conductor
		ViajeFacade vDAO = new ViajeFacade();
		Viaje v = vDAO.find(idViaje);
		v.setEstadoBean(new Estado(1));
		vDAO.edit(v);
		System.out.println(
				"HILO_SIN_RESPUESTA - Viaje " + v.getIdViaje() + " no tiene respuesta, pasa a estado solicitado (1)");
	}

	private void actualizaConductor(int idCond) {
		// Se actualiza historial, aclarando que conductor no responde a la solicitud de
		// viaje
		PosicionFacade cpDAO = new PosicionFacade();
		ConductorPosicion cp = cpDAO.find(idCond);
		cp.setEstado(1);
		cpDAO.edit(cp);
		System.out.println("HILO_SIN_RESPUESTA - Conductor " + cp.getIdPosicion() + " pasa a estado activo (1)");
	}
}