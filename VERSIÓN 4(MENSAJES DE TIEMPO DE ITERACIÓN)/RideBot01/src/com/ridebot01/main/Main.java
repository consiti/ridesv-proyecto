package com.ridebot01.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Main {

	public static void main(String[] args) {

		final Timer timer = new Timer();
		SolicitudViaje solicitud = new SolicitudViaje();

		// FECHA Y HORA DE INICIO
		DateFormat dateForma = new SimpleDateFormat("YYYY-MM-dd' 'HH:mm:ss");
		Date dateInitBot = new Date();
		System.out.println("BOT 1 SE INICIA: " + dateForma.format(dateInitBot) + "\n");

		TimerTask task = new TimerTask() {
			int conta = 0;

			@Override
			public void run() {
				conta++;
				Date dateInitCurrent = new Date();
				System.out.println("BOT-1 ITERACIÓN " + conta);
				System.out.println("INICIO DE ITERACIÓN: " + dateForma.format(dateInitCurrent));
				
				// Verificando solicitudes
				solicitud.atiendeSolicitud();
				
				Date dateFinalCurrent = new Date();
				System.out.println("FINAL DE ITERACIÓN: " + dateForma.format(dateFinalCurrent));
				
				System.out.println("TIEMPO DE ITERACIÓN: " + ((dateFinalCurrent.getTime() / 1000) - (dateInitCurrent.getTime() / 1000)) + "\n");
			}
		};

		timer.schedule(task, 0, 5000);
	}
}