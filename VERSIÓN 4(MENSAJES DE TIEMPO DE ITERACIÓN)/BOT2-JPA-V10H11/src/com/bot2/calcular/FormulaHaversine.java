package com.bot2.calcular;

public class FormulaHaversine {
	
	private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM
	
	/**
	 *  Formula para la navegaci�n astron�mica, en cuanto al c�lculo de la distancia de c�rculo m�ximo entre dos puntos 
	 *  de un globo sabiendo su longitud y su latitud. Es un caso especial de una f�rmula m�s general de 
	 *  trigonometr�a esf�rica, la ley de los semiversenos , que relaciona los lados y �ngulos de los 
	 *  "tri�ngulos esf�ricos"
	 *  
	 *  @return Retrona la disancia en kilometros 
	 * 
	 * */
	
	public static double getDistance(double startLat, double startLong,double endLat, double endLong) {

	double dLat  = Math.toRadians((endLat - startLat));
	double dLong = Math.toRadians((endLong - startLong));
	
	startLat = Math.toRadians(startLat);
	endLat   = Math.toRadians(endLat);
	
	double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
	double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	
	return EARTH_RADIUS * c; // <-- d
	
	}
	
	private static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

}
