package com.bot2.calcular;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bot2.entity.ConductorPosicion;
import com.bot2.entity.Viaje;

public class ConductorMasProximo {
	
	/**
	 * Este metodo realiza el calculo para determinar la distancia mas corta entre los conductores y retorna
	 * un objeto de tipo SolicitudViaje que contiene idConductor y idViaje al cual se Vinculará
	 * @param viaje
	 * @param listaConductorPosicion
	 * @return
	 */
	
	public static SolicitudViaje getConductor(Viaje viaje,List<ConductorPosicion> listaConductorPosicion) {
		
		//Variables
		/*
		 * Posicion Inicial x1 = Latitud y y1 = Longitud
		 * Posicion Final x2 = Latitud y y2 = Longitud
		 *
		 */
		double x1,x2,y1,y2,distanciaFormulaHaversine;
		int contador = 0;
		List<AregloConductores> listaAregloConductores = new ArrayList<AregloConductores>();
		
		//Asignacion de variables Posicion Inicial
		String origenCliente = viaje.getOrigen();
		String[] origenClientePartes = origenCliente.split(",");
		
		x1 = Double.valueOf(origenClientePartes[0]);
		y1 = Double.valueOf(origenClientePartes[1]);		
		
		//Asignacion de variables Posicion Final
		for (int i = 0; i < listaConductorPosicion.size(); i++) {
			String origenConductor = listaConductorPosicion.get(i).getUltimaPosicion();
			String[] origenConductorPartes = origenConductor.split(",");
			
			x2 =  Double.valueOf(origenConductorPartes[0]);
			y2 =  Double.valueOf(origenConductorPartes[1]);
			
			//Calcula la distancia entre las posiciones
			distanciaFormulaHaversine = FormulaHaversine.getDistance(x1, y1, x2, y2);
			
			//Solo tomara los primeros 3 con distancia que sean menor de 5 km
			if(distanciaFormulaHaversine < 5.0) {
				//llena el areglo con las posiciones de todos los conductores
				listaAregloConductores.add(new AregloConductores(listaConductorPosicion.get(i).getIdPosicion(),distanciaFormulaHaversine,x2,y2));
				contador ++;
				if(contador == 3) {
					break;
				}
			}
		}
		
		if(listaAregloConductores.size() > 0) {
			
			//Este areglo contendra las distancias calculado por google map
			AregloConductores[] aregloConductores = new AregloConductores[listaAregloConductores.size()];
			
			//Calcular distancia por google map
			for (int i = 0; i < listaAregloConductores.size(); i++) {
				aregloConductores[i] = new AregloConductores(listaAregloConductores.get(i).getId(),
															GoogleMapsDistance.getDistanceKilometro(x1, y1, listaAregloConductores.get(i).getLatitud(), listaAregloConductores.get(i).getLongitud()),
															listaAregloConductores.get(i).getLatitud(),
															listaAregloConductores.get(i).getLongitud());
			}
			
			//Ordena de menos a mayor las distancias de los conductores 
			Arrays.sort(aregloConductores);
			
			//Prepara la solicitud del viaje
			SolicitudViaje solicitudViaje = new SolicitudViaje();
			solicitudViaje.setIdViaje(viaje.getIdViaje());
			solicitudViaje.setIdConductor(aregloConductores[0].getId());
			
			//Imprime en consola los datos del viaje
			System.out.println("\n----------------SOLICITUD DE VIAJE----------------");
			System.out.println("ID-VIAJE = "+viaje.getIdViaje());
			System.out.println("ID-CLIENTE = "+viaje.getCliente().getIdCliente());
			System.out.println("ID-CONDUCTOR-ASIGNADO = "+aregloConductores[0].getId());
			System.out.println("DISTANCIA-ENTRE-CLIENTE-CONDUCTOR = "+aregloConductores[0].getDistancia()+" kilómetros");
			
			return solicitudViaje;
			
		}else{
			return null;
		}
		
	}

}
