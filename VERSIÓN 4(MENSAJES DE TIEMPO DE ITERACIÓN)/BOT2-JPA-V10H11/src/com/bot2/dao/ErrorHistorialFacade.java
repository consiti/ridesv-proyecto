package com.bot2.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;

import com.bot2.entity.ErrorHistorial;
import com.bot2.util.JpaUtil;

public class ErrorHistorialFacade extends AbstractFacade<ErrorHistorial> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public ErrorHistorialFacade() {
        super(ErrorHistorial.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }

}
