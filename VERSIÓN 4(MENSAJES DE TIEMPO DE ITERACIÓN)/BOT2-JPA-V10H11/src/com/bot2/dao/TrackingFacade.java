package com.bot2.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.bot2.entity.ErrorHistorial;
import com.bot2.entity.Tracking;
import com.bot2.util.JpaUtil;

public class TrackingFacade extends AbstractFacade<Tracking> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public TrackingFacade() {
        super(Tracking.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
    
    
	public List<Tracking> getTrackingByIdviajeAndIdconductorAndEstado(int idViaje,int idConductor,int estadoTracking1 ,int estadoTracking2){
    	List<Tracking> l = null;
    	
		TypedQuery<Tracking> query = em.createQuery("SELECT c FROM Tracking c WHERE c.idConductor = :idConductor AND c.idViaje = :idViaje AND (c.estado = :estado1 OR c.estado = :estado2)", Tracking.class);
		query.setParameter("idViaje", idViaje);
		query.setParameter("idConductor", idConductor);
		query.setParameter("estado1", estadoTracking1);
		query.setParameter("estado2", estadoTracking2);
		
		try {
			l = query.getResultList();
			
			if (l == null) {
				l = new ArrayList<Tracking>();
			}
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade =  new ErrorHistorialFacade();
			ErrorHistorial errorHistorial =  new ErrorHistorial();
			
			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOT2.dao.TrackingFacade.getConductoresByIdviajeAndIdconductorAndEstado");
			
			errorHistorialFacade.create(errorHistorial);
			
			e.printStackTrace();
		}
		
		return l;
		
	}

}
