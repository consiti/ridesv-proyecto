package com.bot2.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.bot2.entity.ErrorHistorial;
import com.bot2.entity.Estado;
import com.bot2.entity.Viaje;
import com.bot2.util.JpaUtil;

public class ViajeFacade extends AbstractFacade<Viaje> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ViajeFacade() {
		super(Viaje.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
		}

		return em;
	}

	public List<Viaje> getViajeByEstado(Estado estado) {
		List<Viaje> list = new ArrayList<Viaje>();

		try {
			TypedQuery<Viaje> q = em.createQuery("SELECT c FROM Viaje c WHERE c.estadoBean = :estado", Viaje.class);
			q.setParameter("estado", estado);

			list = q.getResultList();
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade = new ErrorHistorialFacade();
			
			ErrorHistorial errorHistorial = new ErrorHistorial();
			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOT2.dao.ViajeFacade.getViajeByEstado");

			errorHistorialFacade.create(errorHistorial);
			e.printStackTrace();
		}

		return list;
	}

}
