package com.bot2.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public abstract class AbstractFacade<T> {

	private Class<T> entityClass;

	public AbstractFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	protected abstract EntityManager getEntityManager();

	public void create(T entity) {
		EntityManager em = getEntityManager();

		try {
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
			}
		}
	}

	public void batchCreate(List<T> entities) {
		EntityManager em = getEntityManager();

		try {
			 em.getTransaction().begin();

			for (T entity : entities) {
				em.persist(entity);
			}

			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
			}
		}
	}

	public void edit(T entity) {
		EntityManager em = getEntityManager();

		try {
			em.getTransaction().begin();
			em.merge(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
			}
		}
	}

	public void batchEdit(List<T> entities) {
		EntityManager em = getEntityManager();

		try {
			em.getTransaction().begin();
			
			for (T entity : entities) {
				em.merge(entity);
			}

			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
			}
		}
	}

	public void remove(Object id) {
		EntityManager em = getEntityManager();

		try {
			em.getTransaction().begin();
			em.remove(em.find(entityClass, id));
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
			}
		}
	}

	public void bacthRemove(List<T> entities) {
		EntityManager em = getEntityManager();

		try {
			em.getTransaction().begin();

			for (T entity : entities) {
				em.remove(em.merge(entity));
			}

			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
			}
		}
	}

	public T find(Object id) {
		return getEntityManager().find(entityClass, id);
	}

	public List<T> findAll() {
		CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(entityClass);
		cq.select(cq.from(entityClass));

		return getEntityManager().createQuery(cq).getResultList();
	}

	public List<T> findRange(int[] range) {
		CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(entityClass);
		cq.select(cq.from(entityClass));
		TypedQuery<T> q = getEntityManager().createQuery(cq);
		q.setMaxResults(range[1] - range[0]);
		q.setFirstResult(range[0]);

		return q.getResultList();
	}

	public int count() {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<T> rt = cq.from(entityClass);
		cq.select(cb.count(rt));
		TypedQuery<Long> q = getEntityManager().createQuery(cq);

		return q.getSingleResult().intValue();
	}

}
