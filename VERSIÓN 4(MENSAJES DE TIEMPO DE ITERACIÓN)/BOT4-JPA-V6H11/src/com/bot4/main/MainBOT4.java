package com.bot4.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainBOT4 {

	public static void main(String[] args) {

		final Timer timer = new Timer();
		CoreBOT4 coreBOT4 = new CoreBOT4();

		// FECHA Y HORA DE INICIO
		DateFormat dateForma = new SimpleDateFormat("YYYY-MM-dd' 'HH:mm:ss");
		Date dateInitBot = new Date();
		System.out.println("BOT 4 SE INICIA: " + dateForma.format(dateInitBot) + "\n");

		TimerTask task = new TimerTask() {
			int count = 0;

			@Override
			public void run() {
				count++;
				Date dateInitCurrent = new Date();
				System.out.println("BOT-4 ITERACIÓN " + count);
				System.out.println("INICIO DE ITERACIÓN: " + dateForma.format(dateInitCurrent));
				
				coreBOT4.run();
				
				Date dateFinalCurrent = new Date();
				System.out.println("FINAL DE ITERACIÓN: " + dateForma.format(dateFinalCurrent));
				
				System.out.println("TIEMPO DE ITERACIÓN: " + ((dateFinalCurrent.getTime() / 1000) - (dateInitCurrent.getTime() / 1000)) + "\n");
			}
		};

		timer.schedule(task, 0, 5000);
	}
}
