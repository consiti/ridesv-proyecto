package com.bot4.main;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.bot4.dao.*;
import com.bot4.entity.Conductor;
import com.bot4.entity.ConductorPosicion;
import com.bot4.entity.Correo;
import com.bot4.entity.Estado;
import com.bot4.entity.Viaje;
import com.bot4.email.*;

public class CoreBOT4 {

	private CorreoFacade correoFacade;
	private ConductorFacade conductorFacade;
	private ConductorPosicionFacade conductorPosicionFacade;
	private ViajeFacade viajeFacade;
	private EmailFeedBack email;

	public CoreBOT4() {
		// Instancia de las consultas
		correoFacade = new CorreoFacade();
		conductorFacade = new ConductorFacade();
		conductorPosicionFacade = new ConductorPosicionFacade();
		viajeFacade = new ViajeFacade();
		email = new EmailFeedBack();
	}

	public void run() {
		// Se obtiene la lista de la tabla <Correo> de los correos no leidos que
		// coresponden al bot 4
		List<Correo> listaCorreo = correoFacade.getCorreoByEstado0ANDBot4();

		if (listaCorreo.size() > 0) {

			for (int i = 0; i < listaCorreo.size(); i++) {
				String asuntoCortado[] = listaCorreo.get(i).getAsunto().split("-");

				// Se realizan las consultas
				Conductor conductor = conductorFacade.getConductorByTelefono(asuntoCortado[3]);
				
				if (conductor != null) {
					Viaje viaje = viajeFacade.getViajeByIdConductor(conductor.getIdConductor());
					
					if (viaje != null) {
						// Actualiza el estado de la tabla ConductorPosicion de 2 <Proceso de viaje> a
						// 1 <Activo>
						ConductorPosicion conductorPosicion = conductorPosicionFacade.find(conductor.getIdConductor());
						conductorPosicion.setEstado(1);
						conductorPosicionFacade.edit(conductorPosicion);

						// Salidas en pantalla
						System.out.println("------- SE ACTUALIZO ESTADO CONDUCTOR A ACTIVO -------");
						System.out.println("ID CONDUCTOR = " + conductor.getIdConductor());
						System.out.println("TELEFONO CONDUCTOR = " + conductor.getTelefono());
						System.out.println("NOMBRE CONDUCTOR = " + conductor.getNombre() + "-" + conductor.getApellido() + "\n");

						// Actualiza el estado dela tabla Viaje de 4 <En curso> a 5 <Completo>
						viaje.setEstadoBean(new Estado(5));
						viaje.setHoraFinal(new Time(new Date().getTime()));
						viajeFacade.edit(viaje);
						
						// Salidas en pantalla
						System.out.println("------- SE ACTUALIZO ESTADO VIAJE A COMPLETO -------");
						System.out.println("ID VIAJE = " + viaje.getIdViaje() + "\n");

						// Se Actualiza la tabla del <Correo> como leido
						Correo correo = listaCorreo.get(i);
						correo.setEstado(1);
						correoFacade.edit(correo);

						// Salidas en pantalla
						System.out.println("------- SE ACTUALIZO TABLA DE <correo> COMO LEIDO -------");
						System.out.println("Estado 0 a 1\n");

						// Se envia el correo al cliente para el feedback
						email.sendEmail(viaje, conductor);

						// Salidas en pantalla
						System.out.println("------- SE ENVIO EL CORREO AL CLIENTE PARA EL FEEDBACK -------");
						System.out.println("NOMBRE CLIENTE = " + viaje.getCliente().getNombre());
						System.out.println("CORREO CLIENTE = " + viaje.getCliente().getEmail() + "\n");
						
						System.out.println("-------<***** VIAJE FINALIZADO *****>-------\n");
					} else {
						System.out.println("Problema al Buscar el Viaje X Conductor con el Id Connductor = "
								+ conductor.getIdConductor());
					}

				} else {
					System.out.println("Problema al Bucar el conductor por su telefono = " + asuntoCortado[3]);
				}
			}

		} else {
			System.out.println("No se encontraron correos nuevos para el BOT 4");
		}
	}
}
