package com.bot4.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.bot4.entity.Conductor;
import com.bot4.entity.ErrorHistorial;
import com.bot4.util.JpaUtil;

public class ConductorFacade extends AbstractFacade<Conductor> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ConductorFacade() {
		super(Conductor.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
		}

		return em;
	}

	public Conductor getConductorByTelefono(String telefono) {
		try {
			TypedQuery<Conductor> q = em.createQuery("SELECT c FROM Conductor c WHERE c.telefono = :telefono", Conductor.class);
			q.setParameter("telefono", telefono);
			return q.getSingleResult();
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade = new ErrorHistorialFacade();
			ErrorHistorial errorHistorial = new ErrorHistorial();

			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOT4.dao.ConductorFacade.getConductorByTelefono");

			errorHistorialFacade.create(errorHistorial);

			e.printStackTrace();
			return null;
		}
	}
}
