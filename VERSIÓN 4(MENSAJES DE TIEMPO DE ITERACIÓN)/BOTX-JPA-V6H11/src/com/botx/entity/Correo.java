package com.botx.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the correos database table.
 * 
 */
@Entity
@Table(name="correos")
@NamedQuery(name="Correo.findAll", query="SELECT c FROM Correo c")
public class Correo implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idcorreos;
	private String asunto;
	private String bot;
	private String cuerpo;
	private int estado;

	public Correo() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdcorreos() {
		return this.idcorreos;
	}

	public void setIdcorreos(int idcorreos) {
		this.idcorreos = idcorreos;
	}


	public String getAsunto() {
		return this.asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}


	public String getBot() {
		return this.bot;
	}

	public void setBot(String bot) {
		this.bot = bot;
	}


	@Lob
	public String getCuerpo() {
		return this.cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}


	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

}