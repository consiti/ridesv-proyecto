package com.botx.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainBOTX {

	public static void main(String[] args) {

		final Timer timer = new Timer();
		CoreBOTX coreBOTX = new CoreBOTX();

		// FECHA Y HORA DE INICIO
		DateFormat dateForma = new SimpleDateFormat("YYYY-MM-dd' 'HH:mm:ss");
		Date dateInitBot = new Date();
		System.out.println("BOT X SE INICIA: " + dateForma.format(dateInitBot) + "\n");

		TimerTask task = new TimerTask() {
			int count = 0;

			@Override
			public void run() {
				count++;
				Date dateInitCurrent = new Date();
				System.out.println("BOT-X ITERACIÓN " + count);
				System.out.println("INICIO DE ITERACIÓN: " + dateForma.format(dateInitCurrent));
				
				coreBOTX.run();
				
				Date dateFinalCurrent = new Date();
				System.out.println("FINAL DE ITERACIÓN: " + dateForma.format(dateFinalCurrent));
				
				System.out.println("TIEMPO DE ITERACIÓN: " + ((dateFinalCurrent.getTime() / 1000) - (dateInitCurrent.getTime() / 1000)) + "\n");
			}
		};

		timer.schedule(task, 0, 5000);
	}
}
