package com.ridebot03.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.ridebot03.main.ViajeAceptado;

public class Main {

	public static void main(String[] args) {

		final Timer timer = new Timer();
		ViajeAceptado mail = new ViajeAceptado();

		// FECHA Y HORA DE INICIO
		DateFormat dateForma = new SimpleDateFormat("YYYY-MM-dd' 'HH:mm:ss");
		Date dateInitBot = new Date();
		System.out.println("BOT 3 SE INICIA: " + dateForma.format(dateInitBot) + "\n");

		// Se repite la lectura de los mensajes de inbox cada 5 segs.
		TimerTask task = new TimerTask() {
			int conta = 0;

			@Override
			public void run() {
				conta++;
				Date dateInitCurrent = new Date();
				System.out.println("BOT-3 ITERACIÓN " + conta);
				System.out.println("INICIO DE ITERACIÓN: " + dateForma.format(dateInitCurrent));
				
				mail.atiendeConfirmacion();
				
				Date dateFinalCurrent = new Date();
				System.out.println("FINAL DE ITERACIÓN: " + dateForma.format(dateFinalCurrent));
				
				System.out.println("TIEMPO DE ITERACIÓN: " + ((dateFinalCurrent.getTime() / 1000) - (dateInitCurrent.getTime() / 1000)) + "\n");
			}
		};

		timer.schedule(task, 0, 5000);
	}
}