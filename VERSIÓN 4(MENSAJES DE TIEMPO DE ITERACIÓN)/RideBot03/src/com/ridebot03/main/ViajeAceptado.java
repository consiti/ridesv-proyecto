package com.ridebot03.main;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.ridebot03.dao.*;
import com.ridebot03.entity.Cliente;
import com.ridebot03.entity.Conductor;
import com.ridebot03.entity.ConductorPosicion;
import com.ridebot03.entity.Correo;
import com.ridebot03.entity.ErrorHistorial;
import com.ridebot03.entity.Estado;
import com.ridebot03.entity.Tracking;
import com.ridebot03.entity.Viaje;
import com.ridebot03.util.ReadImage;

public class ViajeAceptado {
	
	private int idViaje;
	private Conductor driver;
	private CorreoFacade correoDAO;
	private ConductorFacade driverDAO;
	private ClienteFacade cliDAO;
	private ViajeFacade ViajeDAO;
	private TrackingFacade trackDAO;
	private PosicionFacade cpDAO;
	private Session session;
	private String username = "info.ridesv2019@gmail.com";
	private String password = "Consiti2018";

	public ViajeAceptado() {
		driver = new Conductor();
		correoDAO = new CorreoFacade();
		driverDAO = new ConductorFacade();
		cliDAO = new ClienteFacade();
		ViajeDAO = new ViajeFacade();
		trackDAO = new TrackingFacade();
		cpDAO = new PosicionFacade();
		getSession();
	}

	public void atiendeConfirmacion() {
		try {
			List<Correo> solicitudes = correoDAO.getUnread("3");
			
			if (!solicitudes.isEmpty()) {
				for (Correo co : solicitudes) {
					String[] data = co.getAsunto().split("-");
					String telCond = data[2];
					idViaje = Integer.parseInt(data[3]);

					co.setEstado(1);
					correoDAO.edit(co);

					int idCliente = actualizaViaje(telCond);
					ConductorPosicion cp = cambiaEstados();
					
					if (cp != null) {
						enviarCorreo(cliDAO.find(idCliente), cp);
					}
				}
			}
		} catch (Exception e) {
			ErrorFacade errorF = new ErrorFacade();
			errorF.create(new ErrorHistorial(0, "CAUSA: " + e.getMessage(), new Date(), "BOT-3 -> ViajeAceptado -> atiendeConfirmacion()"));
			e.printStackTrace();
		}
	}

	public int actualizaViaje(String tel) {
		driver = driverDAO.findByPhone(tel);
		
		Viaje v = ViajeDAO.find(idViaje);
		v.setEstadoBean(new Estado(4));
		v.setConductor(driver);
		
		ViajeDAO.edit(v);
		System.out.println("Viaje " + idViaje + " en curso; asignado a conductor " + driver.getNombre() + " "
				+ driver.getApellido());

		return v.getCliente().getIdCliente();
	}

	public ConductorPosicion cambiaEstados() {
		Tracking td = trackDAO.findRegistroViaje(driver.getIdConductor(), idViaje);
		td.setEstado(2);
		trackDAO.edit(td);
		System.out.println("Estado en tracking: " + td.getEstado());
		
		ConductorPosicion cp = new ConductorPosicion();
		cp = cpDAO.findPosicionConductor(driver.getIdConductor());
		cp.setEstado(2);
		cpDAO.edit(cp);
		System.out.println("Estado de conductor: " + cp.getEstado());
		
		return cp;
	}

	public void getSession() {
		// configuracion para enviar correo
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		
		session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	}
	
	public void enviarCorreo(Cliente c, ConductorPosicion cp) {
		try {
			ViajeFacade ViajeDAO = new ViajeFacade();
			Viaje v = ViajeDAO.find(idViaje);

			String rutaImagen = ReadImage.getImagePath(driver.getVehiculo().getFotografia());

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("info.ridesv2019@gmail.com"));
			message.setSubject("ride-viaje-final-" + driver.getTelefono());
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(driver.getEmail()));
			message.setText("DETALLES DEL CLIENTE:\nNOMBRE: " + c.getNombre() + " " + c.getApellido() + "\n"
					+ "No. CONTACTO: " + c.getTelefono() + "\n"
					+ "ORIGEN: https://www.google.com/maps/dir/?api=1&origin=" + cp.getUltimaPosicion().replace(" ", "")
					+ "&destination=" + v.getOrigen().replace(" ", "") + "&travelmode=driving \n"
					+ "DESTINO: https://www.google.com/maps/dir/?api=1&origin=" + v.getOrigen().replace(" ", "")
					+ "&destination=" + v.getDestino().replace(" ", "") + "&travelmode=driving \n\n"
					+ "Cuando finalice el viaje, conteste TERMINADO a este correo.");

			MimeMessage messageClient = new MimeMessage(session);
			messageClient.setFrom(new InternetAddress(username));
			messageClient.setSubject("RideSV informaci�n de viaje");
			messageClient.addRecipient(Message.RecipientType.TO, new InternetAddress(c.getEmail()));

			MimeMultipart multipart = new MimeMultipart("related");
			BodyPart messageBodyPart = new MimeBodyPart();

			String htmlText = "CONDUCTOR: " + v.getConductor().getNombre() + " " + v.getConductor().getApellido() + "\n"
					+ "TEL�FONO: " + v.getConductor().getTelefono() + "\n" + "N� PLACAS: "
					+ v.getConductor().getVehiculo().getPlaca() + "\n" + "AUTOM�VIL: " + "<br/>"
					+ "<img src=\"cid:image\">";

			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(rutaImagen);

			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<image>");

			multipart.addBodyPart(messageBodyPart);
			messageClient.setContent(multipart);

			Transport.send(message);
			Transport.send(messageClient);

			Path imagesPath = Paths.get(rutaImagen, new String[0]);
			Files.delete(imagesPath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Correo enviado a conductor");
			System.out.println("Correo enviado a cliente");
		}
	}
}
