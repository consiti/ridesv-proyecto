package com.bot2.calcular;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import com.bot2.dao.ErrorHistorialFacade;
import com.bot2.entity.ErrorHistorial;

public class GoogleMapsDistance {
	
	/**
	 * Este metodo devuelve la distancia en kilometros mediante la distancia entre 2 puntos en carro 
	 * por medio de la API de Google Maps 
	 **/
	public static Double getDistanceKilometro(double latitudOringen, double longitudOringen, double latitudDestino, double longitudDestino) {
		
		try {
			
			//Preparacion de la URL para la consulta
			URL url = new URL("https://maps.googleapis.com/maps/api/directions/xml?origin="+String.valueOf(latitudOringen)+","+String.valueOf(longitudOringen)+"&destination="+String.valueOf(latitudDestino)+","+String.valueOf(longitudDestino)+"&travelmode=driving&key=AIzaSyAkRtrup8XxZ3eqjDJPSYfnMKYPmmmQ7h4");
			
			//Reliazacion de la consulta
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			//Se recorre el XML
			String line = "";
			Double distancia = 0.0;
			while ((line = reader.readLine()) != null) {

				//Busca coincidencia <distance>
				if(line.indexOf("</duration>") != -1) {
					line = reader.readLine();
					if(line.indexOf("<distance>") != -1) {
						
						//Se limpia y se prepara el resultado
						line = reader.readLine();
						line = line.replaceAll("<value>", "");
						line = line.replaceAll("</value>", "");
						line = line.replaceAll("\r", "");
						line = line.replaceAll("\n", "");
						line = line.replaceAll(" ", "");
						distancia = Double.parseDouble(line);
	
					}
					
				}
			     
			}
			return distancia/1000;
			
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade =  new ErrorHistorialFacade();
			ErrorHistorial errorHistorial =  new ErrorHistorial();
			
			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOT2.calcular.GoogleMapsDistance.getDistanceKilometro");
			
			errorHistorialFacade.create(errorHistorial);
			
			e.printStackTrace();
			return null;
		}
		
		
	}

}
