package com.bot2.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.bot2.entity.ConductorPosicion;
import com.bot2.entity.ErrorHistorial;
import com.bot2.util.JpaUtil;

public class ConductorPosicionFacade extends AbstractFacade<ConductorPosicion> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ConductorPosicionFacade() {
		super(ConductorPosicion.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
		}

		return em;
	}

	public List<ConductorPosicion> getConductorPosicionByEstado(int estado) {
		List<ConductorPosicion> list = new ArrayList<ConductorPosicion>();

		try {
			TypedQuery<ConductorPosicion> q = em.createQuery("SELECT c FROM ConductorPosicion c WHERE c.estado = :estado", ConductorPosicion.class);
			q.setParameter("estado", estado);
			
			list = q.getResultList();
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade = new ErrorHistorialFacade();

			ErrorHistorial errorHistorial = new ErrorHistorial();
			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOT2.dao.ConductorPosicionFacade.getConductorPosicionByEstado");

			errorHistorialFacade.create(errorHistorial);

			e.printStackTrace();
		}

		return list;
	}

}
