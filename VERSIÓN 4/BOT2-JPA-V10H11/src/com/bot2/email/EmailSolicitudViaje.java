package com.bot2.email;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.bot2.calcular.SolicitudViaje;
import com.bot2.dao.ClienteFacade;
import com.bot2.dao.ConductorFacade;
import com.bot2.dao.ViajeFacade;
import com.bot2.entity.Cliente;
import com.bot2.entity.Conductor;
import com.bot2.entity.Viaje;

import eu.bitm.NominatimReverseGeocoding.NominatimReverseGeocodingJAPI;

public class EmailSolicitudViaje {
	
	private String emailOrigen = "info.ridesv2019@gmail.com";
	private String passwordEmailOrigen = "Consiti2018";
	private Session session;
	
	public EmailSolicitudViaje() {
		getSession();
	}
	
	public void getSession() {
		// configuracion para enviar correo
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailOrigen, passwordEmailOrigen);
			}
		});
		
		System.out.println("Inicio de sesi�n para correo exitosa.");
	}

	public boolean sendEmailImagen(SolicitudViaje solicitudViaje, String rutaImagen) {

		// ------------------------------ CONSULTAS Y PREPARACION DEL CUERPO DEL CORREO
		// ------------------------------

		String asuntoCorreo = "ride-solicitud";
		String cuerpoCorreo = "";

		// Consulta ala Base de datos de la informacion a enviar
		ViajeFacade viajeFacade = new ViajeFacade();
		Viaje viaje = viajeFacade.find(solicitudViaje.getIdViaje());
		ClienteFacade clienteFacade = new ClienteFacade();
		Cliente cliente = clienteFacade.find(viaje.getCliente().getIdCliente());
		ConductorFacade conductorFacade = new ConductorFacade();
		Conductor conductor = conductorFacade.find(solicitudViaje.getIdConductor());

		// Prepara las coordenadas de latitud y longitud para transformarlas en
		// direccion
		String origenCliente = viaje.getOrigen();
		String destinoCliente = viaje.getDestino();
		String[] origenClientePartes = origenCliente.split(",");
		String[] destinoClientePartes = destinoCliente.split(",");
		NominatimReverseGeocodingJAPI reverseGeocoding = new NominatimReverseGeocodingJAPI();

		// Armando el correo con los campos necesarios
		asuntoCorreo = asuntoCorreo + "-" + conductor.getTelefono() + "-" + viaje.getIdViaje();
		cuerpoCorreo = cuerpoCorreo + "-----SOLICITUD DE VIAJE-----<br>";
		cuerpoCorreo = cuerpoCorreo + "PRECIO DEL VIAJE : " + viaje.getPrecio() + "<br>";
		cuerpoCorreo = cuerpoCorreo + "CORREO DEL CLIENTE : " + cliente.getEmail() + "<br>";
		try {
			// Traduce las coordenadas de latitud y longitud a Direccion
			String reverseGeocodingOrigen = reverseGeocoding
					.getAdress(Double.valueOf(origenClientePartes[0]), Double.valueOf(origenClientePartes[1]))
					.toString();
			reverseGeocodingOrigen = new String(reverseGeocodingOrigen.getBytes("ISO-8859-1"), "UTF-8");

			String reverseGeocodingDestino = reverseGeocoding
					.getAdress(Double.valueOf(destinoClientePartes[0]), Double.valueOf(destinoClientePartes[1]))
					.toString();
			reverseGeocodingDestino = new String(reverseGeocodingDestino.getBytes("ISO-8859-1"), "UTF-8");

			cuerpoCorreo = cuerpoCorreo + "DIRECCION ORIGEN-CLIENTE : " + reverseGeocodingOrigen + "\n<br>";
			cuerpoCorreo = cuerpoCorreo + "DIRECCION DESTINO-CLIENTE : " + reverseGeocodingDestino + "\n\n";
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ------------------------------ CONFIGURACION PARA EL ENVIO DE CORREO
		// ------------------------------

		// PARAMETROS PARA EL ENVIO DE CORREO
		String emailDestino = conductor.getEmail();
		String rutaImagenEnviar = rutaImagen;

		try {
			// Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailOrigen));
			message.setSubject(asuntoCorreo);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDestino));

			// This mail has 2 part, the BODY and the embedded image
			MimeMultipart multipart = new MimeMultipart("related");

			// first part (the html)
			BodyPart messageBodyPart = new MimeBodyPart();
			String htmlText = cuerpoCorreo + "<br/><img src=\"cid:image\">";
			messageBodyPart.setContent(htmlText, "text/html");
			// add it
			multipart.addBodyPart(messageBodyPart);

			// second part (the image)
			messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(rutaImagenEnviar);

			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<image>");

			// add image to the multipart
			multipart.addBodyPart(messageBodyPart);

			// put everything together
			message.setContent(multipart);

			// Send message
			Transport.send(message);

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

}
