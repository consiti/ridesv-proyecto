
package com.bot2.main;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Main {

	public static void main(String[] args) {

		final Timer timer = new Timer();
		CoreBOT2 bot = new CoreBOT2();

		TimerTask task = new TimerTask() {
			int cont = 0;

			@Override
			public void run() {
				cont++;
				bot.run();
				System.out.println("BOT-2 Iteración " + cont + " = " + new Date() + "\n");
			}
		};

		timer.schedule(task, 0, 5000);
	}
}
