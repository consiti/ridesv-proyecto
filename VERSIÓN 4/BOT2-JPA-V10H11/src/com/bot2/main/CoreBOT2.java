package com.bot2.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bot2.calcular.ConductorMasProximo;
import com.bot2.calcular.SolicitudViaje;
import com.bot2.dao.ConductorFacade;
import com.bot2.dao.ConductorPosicionFacade;
import com.bot2.dao.TrackingFacade;
import com.bot2.dao.ViajeFacade;
import com.bot2.email.EmailSolicitudViaje;
import com.bot2.entity.Conductor;
import com.bot2.entity.ConductorPosicion;
import com.bot2.entity.Estado;
import com.bot2.entity.Tracking;
import com.bot2.entity.Viaje;

public class CoreBOT2 {

	private final String RUTA_IMAGEN_INSTRUCCIONES_CORREO = "IMAGE-RIDE-EMAIL\\instrucciones-correo.jpg";
	// Consulta a la base de Datos
	private ViajeFacade viajeFacade;
	private ConductorPosicionFacade conductorPosicionFacade;
	private ConductorFacade conductorFacade;
	private TrackingFacade trackingFacade;
	private EmailSolicitudViaje email;

	public CoreBOT2() {
		viajeFacade = new ViajeFacade();
		conductorPosicionFacade = new ConductorPosicionFacade();
		conductorFacade = new ConductorFacade();
		trackingFacade = new TrackingFacade();
		email = new EmailSolicitudViaje();
	}
	
	/*
	 * LOGICA DE CONSULTAR LA BASE Y CALCULAR LA DISTANCIA MAS
	 * CORTA-------------------------------------------------------------
	 */
	public void run() {
		// Busca las solicitudes del viaje y su estado = Solicitado
		for (Viaje cv : viajeFacade.getViajeByEstado(new Estado(1))) {

			// Crea la lista para ser calculada
			List<ConductorPosicion> listaConductorPosicion = new ArrayList<ConductorPosicion>();

			// Busca el conductor cuyo estado este disponible y su vehiculo sea el solicitado
			for (ConductorPosicion cc : conductorPosicionFacade.getConductorPosicionByEstado(1)) {
				Conductor conductor = conductorFacade.find(cc.getIdPosicion());
				
				if (conductor.getVehiculo().getIdTipoVehiculo() == cv.getIdTipoVehiculo()) {

					// Filtro de personas que rechazaron la solicitud de viaje
					List<Tracking> listTracking = trackingFacade.getTrackingByIdviajeAndIdconductorAndEstado(cv.getIdViaje(), cc.getIdPosicion(), 3, 4);
					
					if (listTracking.size() == 0) {
						listaConductorPosicion.add(cc);
					}

				}

			}

			if (listaConductorPosicion.isEmpty()) {
				System.out.println("NO HAY CONDUCTORES DISPONIBLES PARA LA SOLICITUD DE VIAJE ID = " + cv.getIdViaje());
			} else {
				// Busca la ruta mas corta
				SolicitudViaje solicitudViaje = ConductorMasProximo.getConductor(cv, listaConductorPosicion);

				// Filtro de conductores por distancia
				if (solicitudViaje != null) {

					// -----------------------------CAMBIOS EN EL ESTADO DE LA BASE DE DATOS
					// RIDE-----------------------------

					// Cambia el estado del viaje de "Solicitado(estado 1)" a "En proceso(Estado 6)"
					cv.setEstadoBean(new Estado(6));
					viajeFacade.edit(cv);
					// SALIDA EN PANTALLA
					System.out.println("Se cambia el estado del viaje de \"Solicitado\" a \"En proceso\"");

					// Cambia el estado del conductor de "Activo(1)" a "Espera de respuesta(4)"
					ConductorPosicion cc = conductorPosicionFacade.find(solicitudViaje.getIdConductor());
					cc.setEstado(4);
					conductorPosicionFacade.edit(cc);
					// SALIDA EN PANTALLA
					System.out.println("Cambia el estado del conductor de \"Activo\" a \"Espera de respuesta\"");

					// Realiza una inserción en la tabla tracking con el idConductor y el idViaje
					// Con el estado "En espera"
					Tracking tracking = new Tracking();
					tracking.setIdConductor(solicitudViaje.getIdConductor());
					tracking.setIdViaje(solicitudViaje.getIdViaje());
					tracking.setFecha(new Date());
					tracking.setEstado(1);
					trackingFacade.create(tracking);
					// SALIDA EN PANTALLA
					System.out.println(
							"Realiza una inserción en la tabla tracking con el idConductor y el idViaje con el estado \"En espera\"");

					/*---------- ENVIAR EL CORREO HACIA EL CONDUCTOR CON LA PETICIÓN DEL VIAJE ------------*/
					email.sendEmailImagen(solicitudViaje, RUTA_IMAGEN_INSTRUCCIONES_CORREO);
					// SALIDA EN PANTALLA
					System.out.println("Se envio el correo hacia el conductor con el ID = "
							+ solicitudViaje.getIdConductor() + " con la peticion del viaje.\n");

				} else {
					System.out
							.println("TODOS LOS CONDUCTORES ESTAN FUERA DEL RANGO DE LOS 5 KILOMETROS PARA EL VIAJE = "
									+ cv.getIdViaje());
				}

			}

		}

	}

}
