package com.bot2.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the conductor_posicion_historial database table.
 * 
 */
@Entity
@Table(name="conductor_posicion_historial")
@NamedQuery(name="ConductorPosicionHistorial.findAll", query="SELECT c FROM ConductorPosicionHistorial c")
public class ConductorPosicionHistorial implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idHistorial;
	private int estado;
	private Date fechaHora;
	private int idConductor;
	private String posicion;
	private String telefono;

	public ConductorPosicionHistorial() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_historial")
	public int getIdHistorial() {
		return this.idHistorial;
	}

	public void setIdHistorial(int idHistorial) {
		this.idHistorial = idHistorial;
	}


	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_hora")
	public Date getFechaHora() {
		return this.fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}


	@Column(name="id_conductor")
	public int getIdConductor() {
		return this.idConductor;
	}

	public void setIdConductor(int idConductor) {
		this.idConductor = idConductor;
	}


	public String getPosicion() {
		return this.posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}


	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}