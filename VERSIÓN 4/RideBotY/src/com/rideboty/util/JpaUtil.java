package com.rideboty.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	private final static String PERSISTENCE_UNIT_NAME = "RideBotY";
    private static EntityManagerFactory factory;
    
    public static EntityManagerFactory getEntityManager(){
        if(factory == null){
        	//De no existir, se crea conexi�n con la persistencia 
            factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        }
        return factory;
    }
}