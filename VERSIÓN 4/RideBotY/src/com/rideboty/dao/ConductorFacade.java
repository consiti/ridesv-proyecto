package com.rideboty.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.rideboty.entity.Conductor;
import com.rideboty.util.JpaUtil;

public class ConductorFacade extends AbstractFacade<Conductor> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ConductorFacade() {
		super(Conductor.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}

	public Conductor findByNumber(String tel) {
		Query q = em.createNamedQuery("Conductor.findByPhone");
		q.setParameter("telefono", tel);
		return (Conductor) q.getSingleResult();
	}
}