package com.rideboty.main;

public class Main {

	public static void main(String[] args) {
		ViajeRechazado vNeg = new ViajeRechazado();
		ViajeSinRespuesta vNull = new ViajeSinRespuesta();

		// Se ejecutan ambos hilos
		vNeg.start(); // Hilo para viajes rechazados
		vNull.start(); // Hilo para viajes sin respuesta
	}
}