package com.ridebot03.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the error_historial database table.
 * 
 */
@Entity
@Table(name="error_historial")
@NamedQuery(name="ErrorHistorial.findAll", query="SELECT e FROM ErrorHistorial e")
public class ErrorHistorial implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idError;
	private String descripcionError;
	private Date fechaHora;
	private String metodo;

	public ErrorHistorial() {
	}


	public ErrorHistorial(int idError, String descripcionError, Date fechaHora, String metodo) {
		super();
		this.idError = idError;
		this.descripcionError = descripcionError;
		this.fechaHora = fechaHora;
		this.metodo = metodo;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_error")
	public int getIdError() {
		return this.idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}


	@Lob
	@Column(name="descripcion_error")
	public String getDescripcionError() {
		return this.descripcionError;
	}

	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_hora")
	public Date getFechaHora() {
		return this.fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}


	public String getMetodo() {
		return this.metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

}