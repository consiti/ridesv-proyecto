package com.ridebot03.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tracking database table.
 * 
 */
@Entity
@Table(name="tracking")
@NamedQueries({
	@NamedQuery(name="Tracking.findAll", query="SELECT t FROM Tracking t"),
	@NamedQuery(name="Tracking.findRecord", query="SELECT t FROM Tracking t WHERE t.idConductor = :idConductor AND t.idViaje = :idViaje")
})
public class Tracking implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idtracking;
	private int estado;
	private Date fecha;
	private int idConductor;
	private int idViaje;

	public Tracking() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdtracking() {
		return this.idtracking;
	}

	public void setIdtracking(int idtracking) {
		this.idtracking = idtracking;
	}


	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	@Column(name="id_conductor")
	public int getIdConductor() {
		return this.idConductor;
	}

	public void setIdConductor(int idConductor) {
		this.idConductor = idConductor;
	}


	@Column(name="id_viaje")
	public int getIdViaje() {
		return this.idViaje;
	}

	public void setIdViaje(int idViaje) {
		this.idViaje = idViaje;
	}

}