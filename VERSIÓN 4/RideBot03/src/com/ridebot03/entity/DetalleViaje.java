package com.ridebot03.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detalle_viaje database table.
 * 
 */
@Entity
@Table(name="detalle_viaje")
@NamedQuery(name="DetalleViaje.findAll", query="SELECT d FROM DetalleViaje d")
public class DetalleViaje implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idDetalleViaje;
	private String descripcion;
	private Viaje viaje;

	public DetalleViaje() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_viaje")
	public int getIdDetalleViaje() {
		return this.idDetalleViaje;
	}

	public void setIdDetalleViaje(int idDetalleViaje) {
		this.idDetalleViaje = idDetalleViaje;
	}


	@Lob
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	//bi-directional many-to-one association to Viaje
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_viaje")
	public Viaje getViaje() {
		return this.viaje;
	}

	public void setViaje(Viaje viaje) {
		this.viaje = viaje;
	}

}