package com.ridebot03.util;

import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;

public class ReadImage {

	public static String getImagePath(byte[] foto) {
		String rutaImagen;

		try {
			rutaImagen = "IMAGE-RIDE-EMAIL\\NuevaImagen.jpg";

			ByteArrayInputStream bis = new ByteArrayInputStream(foto);
			java.awt.image.BufferedImage bImage2 = ImageIO.read(bis);
			ImageIO.write(bImage2, "jpg", new java.io.File(rutaImagen));
		} catch (Exception e) {
			e.printStackTrace();
			rutaImagen = null;
		}

		return rutaImagen;
	}
}
