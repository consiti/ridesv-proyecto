package com.ridebot03.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.ridebot03.entity.Tracking;
import com.ridebot03.util.JpaUtil;

public class TrackingFacade extends AbstractFacade<Tracking> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public TrackingFacade() {
		super(Tracking.class);
		getEntityManager();
	}
	
	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}
	
	public Tracking findRegistroViaje(int idCond, int idViaje) {
		TypedQuery<Tracking> q = em.createNamedQuery("Tracking.findRecord", Tracking.class);
		q.setParameter("idConductor", idCond);
		q.setParameter("idViaje", idViaje);
		return q.getSingleResult();
	}
}