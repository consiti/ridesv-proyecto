package com.ridebot03.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.ridebot03.entity.Conductor;
import com.ridebot03.util.JpaUtil;

public class ConductorFacade extends AbstractFacade<Conductor> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ConductorFacade() {
		super(Conductor.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}

	public Conductor findByPhone(String tel) {
		TypedQuery<Conductor> q = em.createNamedQuery("Conductor.findByPhone", Conductor.class);
		q.setParameter("telefono", tel);
		return q.getSingleResult();
	}
}