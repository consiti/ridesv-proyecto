package com.ridebot03.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import com.ridebot03.entity.Cliente;
import com.ridebot03.util.JpaUtil;

public class ClienteFacade extends AbstractFacade<Cliente> implements Serializable{
	private static final long serialVersionUID = 1L;
	private EntityManager em;
	
	public ClienteFacade() {
		super(Cliente.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if(em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		
		return em;
	}
}