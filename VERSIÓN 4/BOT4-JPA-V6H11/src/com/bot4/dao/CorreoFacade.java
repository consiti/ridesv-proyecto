package com.bot4.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.bot4.entity.Correo;
import com.bot4.entity.ErrorHistorial;
import com.bot4.util.JpaUtil;

public class CorreoFacade extends AbstractFacade<Correo> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public CorreoFacade() {
		super(Correo.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
		}

		return em;
	}

	public List<Correo> getCorreoByEstado0ANDBot4() {
		List<Correo> l = new ArrayList<Correo>();
		
		try {
			TypedQuery<Correo> q = em.createQuery("SELECT c FROM Correo c WHERE c.estado = :estado AND c.bot = :bot", Correo.class);
			q.setParameter("estado", 0);
			q.setParameter("bot", "4");
			
			l = q.getResultList();
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade = new ErrorHistorialFacade();
			ErrorHistorial errorHistorial = new ErrorHistorial();

			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOT4.dao.CorreoFacade.getCorreoByEstado0ANDBot4");

			errorHistorialFacade.create(errorHistorial);

			e.printStackTrace();
			l = null;
		}
		
		return l;
	}
}
