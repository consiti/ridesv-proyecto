package com.bot4.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.bot4.entity.Conductor;
import com.bot4.entity.ErrorHistorial;
import com.bot4.entity.Estado;
import com.bot4.entity.Viaje;
import com.bot4.util.JpaUtil;

public class ViajeFacade extends AbstractFacade<Viaje> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ViajeFacade() {
		super(Viaje.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
		}

		return em;
	}

	public Viaje getViajeByIdConductor(int idConductor) {
		try {
			TypedQuery<Viaje> q = em.createQuery("SELECT c FROM Viaje c WHERE c.conductor = :conductor AND c.estadoBean = :estado", Viaje.class);
			q.setParameter("conductor", new Conductor(idConductor));
			q.setParameter("estado", new Estado(4));

			return q.getSingleResult();
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade = new ErrorHistorialFacade();
			ErrorHistorial errorHistorial = new ErrorHistorial();

			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOT4.dao.ViajeFacade.getViajeByIdConductor");

			errorHistorialFacade.create(errorHistorial);

			e.printStackTrace();
			return null;
		}
	}
}
