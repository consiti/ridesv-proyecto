package com.bot4.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
	private static final  String PERSISTENCE_UNIT_NAME = "BOT4-JPA-V1-PU";
	private static EntityManagerFactory factory;
	public static EntityManagerFactory getEntityManagerFactory(){	
		if(factory == null) {
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return factory;
	}
}
