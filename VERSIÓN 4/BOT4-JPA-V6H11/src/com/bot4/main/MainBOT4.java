package com.bot4.main;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainBOT4 {

	public static void main(String[] args) {
		final Timer timer = new Timer();
		CoreBOT4 coreBOT4 = new CoreBOT4();

		TimerTask task = new TimerTask() {
			int count = 0;

			@Override
			public void run() {
				count++;
				System.out.println("BOT-4 Iteración " + count + "= " + new Date());
				coreBOT4.run();
			}
		};

		timer.schedule(task, 0, 5000);
	}
}
