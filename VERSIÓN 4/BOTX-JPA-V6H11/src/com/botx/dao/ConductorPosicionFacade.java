package com.botx.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.botx.entity.ConductorPosicion;
import com.botx.entity.ErrorHistorial;
import com.botx.util.JpaUtil;

public class ConductorPosicionFacade extends AbstractFacade<ConductorPosicion> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private EntityManager em;

    public ConductorPosicionFacade() {
        super(ConductorPosicion.class);
        getEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            em = JpaUtil.getEntityManagerFactory().createEntityManager();
        }

        return em;
    }
    
    public List<ConductorPosicion> getConductorPosicionByEstado(int estado) {	
		try {
			TypedQuery<ConductorPosicion> q = em.createQuery("SELECT c FROM ConductorPosicion c WHERE c.estado = :estado", ConductorPosicion.class);
			q.setParameter("estado", estado);
			
			return q.getResultList();
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade =  new ErrorHistorialFacade();
			ErrorHistorial errorHistorial =  new ErrorHistorial();
			
			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOTX.dao.ConductorPosicionFacade.getConductorPosicionByEstado");
			
			errorHistorialFacade.create(errorHistorial);
			
			e.printStackTrace();
			return null ;
		}	
	}
}
