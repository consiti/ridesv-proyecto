package com.botx.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.botx.entity.Correo;
import com.botx.entity.ErrorHistorial;
import com.botx.util.JpaUtil;

public class CorreoFacade extends AbstractFacade<Correo> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public CorreoFacade() {
		super(Correo.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
		}

		return em;
	}

	public List<Correo> getCorreoByEstado0ANDBotX() {
		try {
			TypedQuery<Correo> q = em.createQuery("SELECT c FROM Correo c WHERE c.estado = :estado AND c.bot = :bot",
					Correo.class);
			q.setParameter("estado", 0);
			q.setParameter("bot", "X");

			return q.getResultList();
		} catch (Exception e) {
			ErrorHistorialFacade errorHistorialFacade = new ErrorHistorialFacade();
			ErrorHistorial errorHistorial = new ErrorHistorial();

			errorHistorial.setFechaHora(new Date());
			errorHistorial.setDescripcionError(e.getMessage());
			errorHistorial.setMetodo("BOTX.dao.CorreoFacade.getCorreoByEstado0ANDBotX");

			errorHistorialFacade.create(errorHistorial);

			e.printStackTrace();
			return null;
		}
	}
}
