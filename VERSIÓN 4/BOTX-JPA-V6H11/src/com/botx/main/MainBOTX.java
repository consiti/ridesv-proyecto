package com.botx.main;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainBOTX {

	public static void main(String[] args) {

		final Timer timer = new Timer();
		CoreBOTX coreBOTX = new CoreBOTX();
		System.out.println("INICIO DEL BOT X");

		TimerTask task = new TimerTask() {
			int count = 0;

			@Override
			public void run() {
				count++;
				System.out.println("BOT-X iteración " + count + "= " + new Date());
				coreBOTX.run();
			}
		};
		
		timer.schedule(task, 0, 5000);
	}
}
