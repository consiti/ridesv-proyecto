package com.ridebot01.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the viaje_posicion database table.
 * 
 */
@Entity
@Table(name="viaje_posicion")
@NamedQuery(name="ViajePosicion.findAll", query="SELECT v FROM ViajePosicion v")
public class ViajePosicion implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idPosicion;
	private String destino;
	private int idViaje;
	private String origen;

	public ViajePosicion() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_posicion")
	public int getIdPosicion() {
		return this.idPosicion;
	}

	public void setIdPosicion(int idPosicion) {
		this.idPosicion = idPosicion;
	}


	@Lob
	public String getDestino() {
		return this.destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}


	@Column(name="id_viaje")
	public int getIdViaje() {
		return this.idViaje;
	}

	public void setIdViaje(int idViaje) {
		this.idViaje = idViaje;
	}


	@Lob
	public String getOrigen() {
		return this.origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

}