package com.ridebot01.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.ridebot01.entity.TipoVehiculo;
import com.ridebot01.util.JpaUtil;

public class TipoVehiculoFacade extends AbstractFacade<TipoVehiculo> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public TipoVehiculoFacade() {
		super(TipoVehiculo.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}

		return em;
	}

	// Busca id de tipo de veh�culo tomando como par�metro su nombre
	public TipoVehiculo getIdTypeCar(String tipo) {
		TipoVehiculo tp = new TipoVehiculo();

		try {
			TypedQuery<TipoVehiculo> q = em.createNamedQuery("TipoVehiculo.findByNombre", TipoVehiculo.class);
			q.setParameter("nombre", tipo.trim());
			tp = q.getResultList().get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			tp = null;
		}

		return tp;
	}
}