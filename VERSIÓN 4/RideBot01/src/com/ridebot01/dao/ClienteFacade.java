package com.ridebot01.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.ridebot01.entity.Cliente;
import com.ridebot01.util.JpaUtil;

public class ClienteFacade extends AbstractFacade<Cliente> implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager em;

	public ClienteFacade() {
		super(Cliente.class);
		getEntityManager();
	}

	@Override
	protected EntityManager getEntityManager() {
		if (em == null) {
			em = JpaUtil.getEntityManager().createEntityManager();
		}
		return em;
	}

	// Busca cliente usando como par�metro n�mero telef�nico
	public Cliente findByPhoneNumber(String telefono) {
		try {
			Query q = em.createNamedQuery("Cliente.findByTelefono");
			q.setParameter("telefono", telefono);
			return (Cliente) q.getSingleResult();
		} catch (Exception ex) {
			return null;
		}
	}
}